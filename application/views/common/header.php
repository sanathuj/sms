<!DOCTYPE html>

<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.4 Author: ClipTheme -->

<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->

<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->

<!--[if !IE]><!-->

<html lang="en" class="no-js">

	<!--<![endif]-->

	<!-- start: HEAD -->

	<head>
    <?php
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
		<title>STOCK MANAGEMENT SYSTEM</title>

		<!-- start: META -->

		<meta charset="utf-8" />

		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

		<meta name="apple-mobile-web-app-capable" content="yes">

		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<meta content="" name="description" />

		<meta content="" name="author" />

      

         <link rel="icon" type="image/png" href="<?php echo base_url(); ?>thems/images/your-logo-here.png" />

		<!-- end: META -->

		<!-- start: MAIN CSS -->

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap/css/bootstrap.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/font-awesome/css/font-awesome.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>fonts/style.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>css/main.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>css/main-responsive.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/skins/all.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/perfect-scrollbar/src/perfect-scrollbar.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>css/theme_light.css" type="text/css" id="skin_color">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>css/print.css" type="text/css" media="print"/>

		<!--[if IE 7]>

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/font-awesome/css/font-awesome-ie7.min.css">

		<![endif]-->

		<!-- end: MAIN CSS -->

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->

		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

	</head>

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->

		<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/select2/select2.css" />

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/DataTables/media/css/DT_bootstrap.css" />

		<link href="<?php echo asset_url(); ?>plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>

		<link href="<?php echo asset_url(); ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">





		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datepicker/css/datepicker.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/jQuery-Tags-Input/jquery.tagsinput.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/summernote/build/summernote.css">



		<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>css/bootstrap-datetimepicker.css">



		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/gritter/css/jquery.gritter.css">

		<style type="text/css">

			body .modal {

	    /* new custom width

	    width: 750px; */

	    /* must be half of the width, minus scrollbar on the left (30px) 

	    margin-left: -375px;*/

			}

			</style>

<?php clearstatcache();?>