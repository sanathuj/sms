	<?php $this->load->view("common/header"); ?>
	<!-- end: HEAD -->
       
        
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/DataTables/media/css/DT_bootstrap.css" />
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        
 		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/summernote/build/summernote.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->       
        

	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: HEADER -->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<!-- start: TOP NAVIGATION CONTAINER -->
			<div class="container">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="clip-list-2"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<?php $this->load->view("common/logo"); ?>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<!-- start: TOP NAVIGATION MENU -->
				<?php $this->load->view("common/notifications.php"); ?>
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<div class="navbar-content">
				<!-- start: SIDEBAR -->
				<?php $this->load->view("common/navigation"); ?>
				<!-- end: SIDEBAR -->
			</div>
			<!-- start: PAGE -->
			<div class="main-content">
				<!-- start: PANEL CONFIGURATION MODAL FORM -->
				<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Panel Configuration</h4>
							</div>
							<div class="modal-body">
								Here will be a configuration form
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary">
									Save changes
								</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<a href="<?php echo base_url('dashboard'); ?>">
										 Dashboard 
									</a>
								</li>
                                <li>
									<a href="#">
										 Product 
									</a>
								</li>
                                
								<li class="active">
									Edit Product
								</li>
								<li class="search-box">
									<form class="sidebar-search">
										<div class="form-group">
											<input type="text" placeholder="Start Searching...">
											<button class="submit">
												<i class="clip-search-3"></i>
											</button>
										</div>
									</form>
								</li>
							</ol>
							<div class="page-header">
								<h1>Edit Product</h1>
							</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->
						</div>
					</div>
                    <!-- start grid -->
                    <div class="row">
						<div class="col-md-12">
							<!-- start: DYNAMIC TABLE PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Edit Product
								</div>
                                
                                <div class="panel-body">
								    <div id="error"></div>
								<?php 
										$config = array('role' =>'form', 'class'=>'form-horizontal','id'=>'add_product_form', 'name'=>'add_product_form');
										echo form_open_multipart("#",$config);
										?>
										<input type="hidden" name="product_id" value="<?php echo $product_details->product_id; ?>">                                                                   
										<div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-1">
												Product Name
											*</label>
											<div class="col-sm-9">
												<input type="text" id="product_name" class="form-control" name="product_name" value="<?php echo $product_details->product_name; ?>">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Product Code
											*</label>
											<div class="col-sm-9">
												<input type="text" readonly id="product_code" class="form-control" value="<?php echo $product_details->product_code; ?>" name="product_code">
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												SAP Code
											</label>
											<div class="col-sm-9">
												<input type="text" id="product_part_no" class="form-control" name="product_part_no" value="<?php echo $product_details->product_part_no; ?>">
											</div>
										</div>
                                        
									  <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-3" >
												Category*
											</label>
											<div class="col-sm-9">
												<select class="form-control search-select" id="category" name="category">
												<option value="">&nbsp;</option>
													<?php foreach ($main_category as $key => $category) {
														if ($product_details->cat_id == $category->cat_id) {
														echo "<option selected value='$category->cat_id; ?>'>$category->cat_name</option>";										
														} else {
														echo "<option value='$category->cat_id'>$category->cat_name</option>";										
														}
													} ?>
												</select>
                                        
											</div>
										</div>
 								<div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-3" >
												Sub Category
											</label>
											<div id="subcat_data" class="col-sm-9">
											<select data-placeholder="Select Category to load Subcategories" id="subcategory" class="form-control search-select" name="subcategory">
													<?php foreach ($sub_category as $key => $sub_category) {
														if ($product_details->sub_cat_id == $sub_category->sub_cat_id) {
														echo "<option selected value='$sub_category->sub_cat_id; ?>'>$sub_category->sub_cat_name</option>";										
														} else {
														echo "<option value='$sub_category->sub_cat_id'>$sub_category->sub_cat_name</option>";										
														}
													} ?>
											</select>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
											Product Unit *
											</label>
											<div class="col-sm-9">
												<select id="form-field-select-3" class="form-control search-select" id="unit" name="unit">
					                                <option value="">&nbsp;</option>
						                            <?php foreach ($unit_type as $key => $unit) {
						                            	if ($product_details->product_unit == $unit->unit_id) {
						                            	echo "<option selected value='$unit->unit_id'>$unit->unit_code</option>";
						                            	} else {
						                            	echo "<option value='$unit->unit_id'>$unit->unit_code</option>";
						                            	}
						                            	
						                            } ?>  
					                             </select>
											</div>
										</div>
                                        
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Product Price *
											</label>
											<div class="col-sm-9">
												<input type="text" id="product_price" class="form-control auto" name="product_price" data-a-sign="Rs. " data-d-group="2" value="<?php echo $product_details->product_price; ?>">
											</div>
										</div>
                                         <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Wholesale Price *
											</label>
											<div class="col-sm-9">
												<input type="text" id="wholesale_price" class="form-control auto" name="wholesale_price" data-a-sign="Rs. " data-d-group="2" value="<?php echo $product_details->wholesale_price; ?>">
											</div>
										</div>   
                                         <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Credit Selling Price 
											</label>
											<div class="col-sm-9">
												<input type="text" id="credit_salling_price" class="form-control auto" name="credit_salling_price" data-a-sign="Rs. " data-d-group="2" value="<?php echo $product_details->credit_salling_price; ?>">
											</div>
										</div> 
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Product Cost
											</label>
											<div class="col-sm-9">
												<input type="text" name="product_cost" id="product_cost" class="form-control auto" data-a-sign="Rs. " data-d-group="2" value="<?php echo $product_details->product_cost; ?>">
											</div>
										</div>
                                                                       
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Tax Method
											</label>
											<div class="col-sm-9">
												 <select id="form-field-select-3" class="form-control" id="tax" name="tax">                             
						                            <?php foreach ($tax as $key => $tax) {
						                            	if ($product_details->tax == $tax->id) {
						                            			echo "<option selected value='$tax->id'>$tax->name</option>";
						                            	} else {
						                            			echo "<option value='$tax->id'>$tax->name</option>";
						                            	}
						                            	
						                            } ?>  
					                             </select>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Minimum Quantity
											</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-2" id="alert_quty" name="alert_quty" class="form-control" value="<?php echo $product_details->product_alert_qty; ?>">
											</div>
										</div>
                                         <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Maximum Quantity
											</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-2" id="product_max_qty" name="product_max_qty" class="form-control" value="<?php echo $product_details->product_max_qty; ?>">
											</div>
										</div>
                                         <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Store Position
											</label>
											<div class="col-sm-9">
                                             <input type="text" id="store_position" class="form-control" name="store_position" value="<?php echo $product_details->store_position; ?>">
											
											
											</div>
										</div> 
                                       
                                        <div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-2">
												Product Image
											</label>
											<div class="col-sm-9">
												<div class="form-group">
											<div class="col-sm-4">
												<label>
													
												</label>
												<div class="fileupload fileupload-new" data-provides="fileupload">
													<div class="input-group">
														<div class="form-control uneditable-input">
															<i class="fa fa-file fileupload-exists"></i>
															<span class="fileupload-preview"></span>
														</div>
														<div class="input-group-btn">
															<div class="btn btn-light-grey btn-file">
																<span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Select file</span>
																<span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Change</span>
																<input type="file" class="file-input" name="userfile">
															</div>
															<a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
																<i class="fa fa-times"></i> Remove
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
											</div>
										</div>
                                        
                                        

										<div class="form-group">
                                        <div class="col-sm-12">
												<label class="control-label">
													Product Details
												</label>
												<textarea class="ckeditor form-control" cols="10" rows="10" name="product_details" ><?php echo $product_details->product_details; ?></textarea>
                                         </div>
										</div>
                                     <div class="form-group">
                                     <div class="col-sm-12">       
									<button class="btn btn-primary btn-squared" type="submit">
											Update Product
										</button>
                                        </div>
                                        </div>
									</form>
								</div>
							</div>
							</div>
							<!-- end: DYNAMIC TABLE PANEL -->
						</div>
					</div>
                    <!-- end grid -->
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<div class="footer clearfix">
			<div class="footer-inner">
				2014 &copy; clip-one by cliptheme.
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
		<!-- start: RIGHT SIDEBAR -->
		<div id="page-sidebar">
			<a class="sidebar-toggler sb-toggle" href="#"><i class="fa fa-indent"></i></a>
			<div class="sidebar-wrapper">
				<ul class="nav nav-tabs nav-justified" id="sidebar-tab">
					<li class="active">
						<a href="#users" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a>
					</li>
					<li>
						<a href="#favorites" role="tab" data-toggle="tab"><i class="fa fa-heart"></i></a>
					</li>
					<li>
						<a href="#settings" role="tab" data-toggle="tab"><i class="fa fa-gear"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<!-- end: RIGHT SIDEBAR -->
		<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title">Event Management</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-light-grey">
							Close
						</button>
						<button type="button" class="btn btn-danger remove-event no-display">
							<i class='fa fa-trash-o'></i> Delete Event
						</button>
						<button type='submit' class='btn btn-success save-event'>
							<i class='fa fa-check'></i> Save
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<?php $this->load->view("common/footer"); ?>
		<!-- end: MAIN JAVASCRIPTS -->
        
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY VALIDATION-->
		<script src="<?php echo asset_url(); ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo asset_url(); ?>js/autoNumeric.js"></script>
		<script src="<?php echo asset_url(); ?>js/form-validation-add-product.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY  VALIDATION-->
                        
		<script>
			jQuery(document).ready(function() {
				$(".search-select").select2({
					allowClear: true
				});

				FormValidator.init();
    			$('.auto').autoNumeric('init');
			});


			$('select#category').on('change', function(){

			   		var v = $(this).val();

					$.ajax({
					  type: "get",
					  async: false,
					  url: "<?php echo base_url('Products/get_sub_category_by_id'); ?>",
					  data: { category_id: v },
					  dataType: "html",
					  success: function(data) {
						if(data != "") {
							$('#subcat_data').empty();
							$('#subcat_data').html(data);
							$("#subcategory").select2({allowClear: true});
						} else {
							$('#subcat_data').empty();
							var default_data = '<select name="subcategory" id="subcategory" class="form-control search-select" data-placeholder="Select Category to load Subcategories"></select>';
							$('#subcat_data').html(default_data);
							$("#subcategory").select2({allowClear: true});
							set_message("Product Info","No Subcategory found for the select category.");
						}},
					  error: function(){
       					alert('Error occured while getting data from server.');
    				  }
					  
					});
			});


		function add_product(form) {

               $('body').modalmanager('loading');
                setTimeout(function () {
                    $.ajax({
                    url: "<?php echo base_url('products/edit_product'); ?>", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: new FormData(form), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                        success: function(data)   // A function to be called if request succeeds
                        {
                            var obj = jQuery.parseJSON(data);
                            if (obj.status==0) 
                                {
                                    $('div#error').html('<div class="alert alert-block alert-danger fade in"><button type="button" class="close" data-dismiss="alert">×</button><h4 class="alert-heading"><i class="fa fa-times-circle"></i> Error!</h4>'+obj.validation+'</div>');
                                    $('body').modalmanager('removeLoading');
                                    $('body').attr('class','');
                                } 
                                else
                                {
                                    $('body').modalmanager('removeLoading');
                                    $('body').attr('class','');
                                    set_message('categories notice!','Product successfully Added');
									//document.getElementById("add_product_form").reset();
                                };

                        }
                    });
                }, 1000);
		}

		</script>
	</body>
	<!-- end: BODY -->
</html>