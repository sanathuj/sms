
<!--onLoad="window.print()"-->
<body >
	<?php $this->load->view("common/header"); ?>
	<style type="text/css">
.report_view_th{
	color:#000 !important;
	font-size:12px;	
}

.table-responsive td{
	font-size:11px;
	background-color:#fff !important;	
}
h4{
	font-size:13px;
}
body{
	background-color:#fff !important;
}
</style>
		

<div class="">

         

         
            <div class="modal-body">

 <div style="top:2px;" class="panel-tools open">
<button data-toggle="dropdown" class="btn btn-blue dropdown-toggle" onClick="window.print()">
<i class="fa fa-print"></i>
</button>
</div>
<div class="row">
<div class="col-xs-12 text-center">


  <img src="<?php echo asset_url(); ?>images/print_logo.jpg" width="70%" height="auto">
   </div><!--col-xs-12-->
</div><!--row-->  
 <br>
<br> 

<div style="margin-bottom:15px;" class="row">
<div class="col-xs-6">


</div>
<div class="col-xs-12 pull-left">

<h4 style="margin-top:10px;">Report : Sales Report</h4>
<p>From Date : <?php echo $srh_from_date_dis ?></p>
<p>To Date : <?php echo $srh_to_date_dis ?></p>
</div>
</div>          


<div class="table-responsive">


<table class="table table-bordered table-hover table-striped print-table order-table">
<thead>
<tr class="report_view_th">
<th class="col-sm-1 text-center">No</th>
<th class="text-center">Date</th>
<th class="text-center">Invoice No</th>
<th class="text-center">Customer</th>
<th class="text-center">Payment Status</th>
<th class="text-center">Grand Total</th>
<th class="text-center">Paid</th>
<th class="text-center">Balance</th>

</tr>
</thead>
<tbody>
 <?php 
 //print_r($sales_list);
 $tmpcount=0;
 $tt_grand_tot=0;
  $tt_paid=0;
   $tt_balance=0;
  foreach ($sales_list as $row)
 {
	 $tmpcount++;
	 $total_paid_amount=$row['total_paid_amount'];
	 if (empty($total_paid_amount)) {
		  $pay_st = 'Pending';
		}else{
		  if ($total_paid_amount >= $row['sale_total']) {
			$pay_st = 'Paid';
		  }else{
			$pay_st = 'Partial';
		  }
		}
		$tt_grand_tot=$tt_grand_tot+$row['sale_total'];
		$tt_paid=$tt_paid+$total_paid_amount;
		$tt_balance=$tt_balance+($row['sale_total']-$total_paid_amount);
		
 ?>  
<tr>
<td style="text-align:right; width:40px; vertical-align:middle;"><?php echo sprintf("%04d",$tmpcount); ?></td>
<td style="vertical-align:middle;">
 <?php echo display_date_time_format($row['sale_datetime']); ?> </td>
<td><?php echo $row['sale_reference_no']; ?></td>
<td><?php echo $row['cus_name']; ?></td>
<td> <?php echo $pay_st; ?></td>

<td class="text-right"> <?php echo number_format($row['sale_total'], 2, '.', ','); ?></td>
<td class="text-right"> <?php echo number_format($total_paid_amount, 2, '.', ','); ?></td>
<td class="text-right"> <?php echo number_format($row['sale_total']-$total_paid_amount, 2, '.', ','); ?></td>

</tr>
<?php }?>
</tbody>
<tfoot>
<tr class="report_view_th">
<th colspan="5"></th>

<th class="text-right"><?php echo number_format($tt_grand_tot, 2, '.', ','); ?></th>
<th class="text-right"><?php echo number_format($tt_paid, 2, '.', ','); ?></th>
<th class="text-right"><?php echo number_format($tt_balance, 2, '.', ','); ?></th>

</tr>
</tfoot>
</table>
</div>




<div class="row">
<div class="col-xs-12">
</div>
<div class="col-xs-5 pull-right">
<div class="well-sm">
<p>
Created by:  <?php echo $this->session->userdata('ss_user_first_name'); ?> (<?php echo $this->session->userdata('ss_user_group_name'); ?>)</p> <p>
Date: <?php echo display_date_time_format(date("Y-m-d H:i:s")); ?> </p>
</div>
</div>
</div>

             
                 <!--/.col-md-12-->

</body>

