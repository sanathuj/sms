<?php $this->load->view("common/header"); ?>
<div style="top:10px;" class="panel-tools open">
<button data-toggle="dropdown" class="btn btn-blue dropdown-toggle" onClick="window.print()">
<i class="fa fa-print"></i>
</button>
</div><!--onLoad="window.print()-->
<body>


<style type="text/css">
.report_view_th{
	background-color:#428bca;
	color:#fff !important;	
}
body{
	background-color:#fff !important;
}
</style>
		<style type="text/css">
			body .modal {
	    /* new custom width */
	    width: 750px;
	    /* must be half of the width, minus scrollbar on the left (30px) */
	    margin-left: -375px;
			}
			</style>

<div class="modal-header">

<?php $this->load->view("common/report_header.php"); ?>

            
<div class="well well-sm">
<div class="row bold">
<div class="col-xs-5">
<p class="bold">
Reference: <?php echo $sale_details['sale_reference_no']; ?><br>
Date: <?php echo $sale_details['sale_datetime']; ?><br>
Sale Status: <?php echo $sale_details['sale_status']; ?><br>
Payment Status: <?php echo $sale_details['payment_status']; ?> </p>
</div>
<div class="col-xs-7 text-right">

</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
            </div>
         
            <div class="modal-body">
            
            

<div style="margin-bottom:15px;" class="row">
<div class="col-xs-6">
From:
<h4 style="margin-top:10px;"><?php echo $warehouse_details['name']; ?></h4>
<p>
<?php echo $warehouse_details['address']; ?></p>
Tel: <?php echo $warehouse_details['phone']; ?><br>
Email: <?php echo $warehouse_details['email']; ?></div>
<div class="col-xs-6">
To:<br>
<h4 style="margin-top:10px;"><?php echo $customer_details['cus_name']; ?></h4>
<p><?php echo $customer_details['cus_address']; ?></p>Tel: <?php echo $customer_details['cus_phone']; ?><br>Email: <?php echo $customer_details['cus_email']; ?> </div>
</div>

<div class="table-responsive">

<table class="table table-bordered table-hover table-striped print-table order-table">
<thead>
<tr class="report_view_th">
<th>No</th>
<th>Description</th>
<th>Quantity</th>
<th>Unit Price</th>
<th>Discount (%)</th> 
<th>Subtotal</th>
</tr>
</thead>
<tbody>
 <?php 
 $tmpcount=0;
  foreach ($sale_item_list as $row)
 {
	 $tmpcount++;
 ?>  
<tr>
<td style="text-align:center; width:40px; vertical-align:middle;"><?php echo $tmpcount ?></td>
<td style="vertical-align:middle;">
 <?php echo $row['product_name']; ?> (<?php echo $row['product_code']; ?>)</td>
<td style="width: 80px; text-align:center; vertical-align:middle;"><?php echo $row['quantity']; ?></td>
<td style="text-align:right; width:100px;"><?php echo $row['unit_price']; ?></td>
<td style="width: 100px; text-align:right; vertical-align:middle;"> <?php echo $row['discount']; ?></td> <td style="text-align:right; width:120px;"><?php echo $row['gross_total']; ?></td>
</tr>
<?php }?>
</tbody>
<tfoot>
<tr>
  <td style="text-align:right; padding-right:10px; font-weight:bold;" colspan="5">Order Discount</td>
  <td style="text-align:right; padding-right:10px; font-weight:bold;"><?php echo number_format($sale_details['sale_inv_discount_amount'], 2, '.', ',') ?></td>
</tr>
<tr>
<td style="text-align:right; font-weight:bold;" colspan="5">Total Amount 
</td>
<td style="text-align:right; padding-right:10px; font-weight:bold;"><?php echo number_format($sale_details['sale_total'], 2, '.', ',') ?></td>
</tr>
<tr>
<td style="text-align:right; font-weight:bold;" colspan="5">Paid 
</td>
<td style="text-align:right; font-weight:bold;"><?php echo number_format($sale_details['sale_paid'], 2, '.', ',') ?></td>
</tr>
<tr>
<td style="text-align:right; font-weight:bold;" colspan="5">Balance 
</td>
<td style="text-align:right; font-weight:bold;"><?php echo number_format($sale_details['sale_balance'], 2, '.', ',') ?></td>
</tr>
</tfoot>
</table>
</div>

<div class="row">
<div class="col-xs-12">
</div>
<div class="col-xs-5 pull-right">
<div class="well well-sm">
<p>
Created by:  <?php echo $this->session->userdata('ss_user_first_name'); ?> (<?php echo $this->session->userdata('ss_user_group_name'); ?>)<br>
Date: <?php echo display_date_time_format($sale_details['sale_datetime_created']); ?> </p>
</div>
</div>
</div>

<div class="buttons">
<div class="btn-group btn-group-justified">


<!--<div class="btn-group">
<a title="" class="tip btn btn-primary" href="http://sma.tecdiary.org/sales/pdf/15" data-original-title="Download as PDF">
<i class="fa fa-print"></i>
<span class="hidden-sm hidden-xs">PRINT</span>
</a>
</div>-->


</div>
</div>


                       
                 <!--/.col-md-12-->
                 
 </body>



