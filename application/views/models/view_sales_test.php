
<!DOCTYPE html>
<html>

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>Dynamic Styles: Page Breaking</title>
<style type="text/css" media="print">
div.page {
	page-break-after:always;
}
</style>
</head>

<body onLoad="window.print()">


<div class="page">
	. 
	content on page 1 
	. 
</div>

<div class="page">
	. 
	content on page 2 
	. 
</div>

</body>

</html>