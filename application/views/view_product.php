	<?php $this->load->view("common/header");?>

	<!-- end: HEAD -->

       

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->

		<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/select2/select2.css" />

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/DataTables/media/css/DT_bootstrap.css" />

		<link href="<?php echo asset_url(); ?>plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>

		<link href="<?php echo asset_url(); ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">





		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datepicker/css/datepicker.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/jQuery-Tags-Input/jquery.tagsinput.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">

		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/summernote/build/summernote.css">

		<style type="text/css">

			.table > thead:first-child > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table-striped thead tr.primary:nth-child(2n+1) th {

			    background-color: #428bca;

			    border-color: #357ebd;

			    border-top: 1px solid #357ebd;

			    color: white;

			    text-align: center;

			}



			.box .box-content {

			    background: white none repeat scroll 0 0;

			    padding: 20px;

			}

		</style>



		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

	<!-- end: HEAD -->

	<!-- start: BODY -->

	<body>

		<!-- start: HEADER -->

		<div class="navbar navbar-inverse navbar-fixed-top">

			<!-- start: TOP NAVIGATION CONTAINER -->

			<div class="container">

				<div class="navbar-header">

					<!-- start: RESPONSIVE MENU TOGGLER -->

					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">

						<span class="clip-list-2"></span>

					</button>

					<!-- end: RESPONSIVE MENU TOGGLER -->

					<!-- start: LOGO -->

					<a class="navbar-brand" href="index.html">

						CLIP<i class="clip-clip"></i>ONE

					</a>

					<!-- end: LOGO -->

				</div>

				<div class="navbar-tools">

					<!-- start: TOP NAVIGATION MENU -->

				<?php $this->load->view("common/notifications.php"); ?>

					<!-- end: TOP NAVIGATION MENU -->

				</div>

			</div>

			<!-- end: TOP NAVIGATION CONTAINER -->

		</div>

		<!-- end: HEADER -->

		<!-- start: MAIN CONTAINER -->

		<div class="main-container">

			<div class="navbar-content">

				<!-- start: SIDEBAR -->

				<?php $this->load->view("common/navigation"); ?>

				<!-- end: SIDEBAR -->

			</div>

			<!-- start: PAGE -->

			<div class="main-content">

				<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				<div class="container">

					<!-- start: PAGE HEADER -->

					<div class="row">

						<div class="col-sm-12">

							<!-- start: PAGE TITLE & BREADCRUMB -->

							<ol class="breadcrumb">

								<li>

									<a href="<?php echo base_url('dashboard'); ?>">

										 Dashboard 

									</a>

								</li>

                                <li>

									<a href="<?php echo base_url('products'); ?>">

										 Product 

									</a>

								</li>

                                

								<li class="active">

									Grapefruit

								</li>

								<li class="search-box">

									<form class="sidebar-search">

										<div class="form-group">

											<input type="text" placeholder="Start Searching...">

											<button class="submit">

												<i class="clip-search-3"></i>

											</button>

										</div>

									</form>

								</li>

							</ol>

							<div class="page-header">

								<h1>Product</h1>

							</div>

						</div>

					</div>

					<!-- end: PAGE HEADER -->

					<!-- start: PAGE CONTENT -->

					<div class="row">

						<div class="col-sm-12">

							<div class="tabbable">

								<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">

									<li class="active">

										<a data-toggle="tab" href="#panel_overview">

											Product Details

										</a>

									</li>

								</ul>

								<div class="tab-content">



									<div id="panel_overview" class="tab-pane in active">

											<div class="panel panel-default">

											<div class="panel-heading">Grape fruit</div>

											<div class="panel-body">



										<div class="box-content">

											<div class="row">

											<div class="col-lg-12">

											<div class="row">

											<div class="col-sm-5">

											<img class="img-responsive img-thumbnail" alt="<?php echo $product_details->product_name; ?>" 

											src="<?php echo asset_url().'uploads/'.$product_details->product_image; ?>">

											<div class="padding10" id="multiimages">

											<div class="clearfix"></div>

											</div>

											</div>

											<div class="col-sm-7">

											<div class="table-responsive">

											<table class="table table-borderless table-striped dfTable table-right-left">

											<tbody>

											<tr>

											<td style="width:30%;">Barcode</td>

											<td style="width:70%;">

											<img class="pull-left" alt="<?php echo $product_details->product_code; ?>" src="<?php echo base_url().'products/gen_barcode/'.$product_details->product_code.'/40'; ?>">

											</td>

											</tr>

											<tr>

											<td>Product Name</td>

											<td><?php echo $product_details->product_name; ?></td>

											</tr>

											<tr>

											<td>Product Code</td>

											<td><?php echo ucfirst($product_details->product_code); ?></td>

											</tr>

											<tr>

											<td>Category</td>

											<td><?php echo $product_details->cat_name; ?></td>

											</tr>

											<tr>

											<td>Product Unit</td>

											<td><?php echo $product_details->unit_name; ?></td>

											</tr>

											<tr><td>Product Cost</td><td><?php echo $product_details->product_cost; ?></td></tr>

											<tr><td>Product Price</td><td><?php echo $product_details->product_price; ?></td></tr>

											<tr>

											<td>Tax Rate</td>

											<td><?php echo $product_details->name; ?> <?php echo $product_details->rate; ?>%</td>

											</tr>

											<tr>

											<td>Alert Quantity</td>

											<td><?php echo $product_details->product_alert_qty; ?></td>

											</tr>

											</tbody>

											</table>

											</div>

											</div>

											<div class="clearfix"></div>

											<div class="col-sm-12">

											<div class="row">

											<div class="col-sm-5">

											<h3 class="bold">Warehouse Quantity</h3>

											<div class="table-responsive">

											<table class="table table-bordered table-striped table-condensed dfTable two-columns">

											<thead>

											<tr>

											<th>Warehouse Name</th>

											<th>Quantity (Racks)</th>

											</tr>

											</thead>

											<tbody>

											<?php

											if (empty($warehouses)) { echo "<tr><td>No Found !</td></tr>";} else {foreach ($warehouses as $key => $wh) { 

												echo "<tr><td>$wh->name ($wh->code)</td><td><strong>$wh->quantity</strong></td></tr>";

												}

										    }

											?>

											</tbody>

											</table>

											</div>

											</div>

											<div class="col-sm-7">

											</div>

											</div>

											</div>

												<div class="col-sm-12">

													<div class="panel panel-default">

														<div class="panel-heading">Product Details</div>

															<div class="panel-body"><p><?php echo $product_details->product_details; ?></p>

															</div>

														</div>

													</div>

												</div>

											<div class="buttons">

											<div class="btn-group btn-group-justified">

											<div class="btn-group">

												<a title="" class="tip btn btn-primary" href="#" onclick="print_barcode(<?php echo $product_details->product_id; ?>); return false;" data-original-title="Barcode">

												<i class="fa fa-print"></i> <span class="hidden-sm hidden-xs">Print Barcode</span>

												</a>

												</div>

												<div class="btn-group">

												<a title="" class="tip btn btn-warning tip" href="<?php echo base_url(); ?>products/edit/<?php echo $product_details->product_id; ?>" data-original-title="Edit Product">

												<i class="fa fa-edit"></i> <span class="hidden-sm hidden-xs">Edit</span>

												</a>

												</div>

											</div>

											</div>

								</div>

											</div>

											</div>

									</div>

									</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					<!-- end: PAGE CONTENT-->

                    

					

			</div>

			<!-- end: PAGE -->

		</div>

		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->

		<div class="footer clearfix">

			<div class="footer-inner">

				2014 &copy; clip-one by cliptheme.

			</div>

			<div class="footer-items">

				<span class="go-top"><i class="clip-chevron-up"></i></span>

			</div>

		</div>

		<!-- end: FOOTER -->

		<!-- start: RIGHT SIDEBAR -->

		<!-- end: RIGHT SIDEBAR -->

		<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">

							&times;

						</button>

						<h4 class="modal-title">Event Management</h4>

					</div>

					<div class="modal-body"></div>

					<div class="modal-footer">

						<button type="button" data-dismiss="modal" class="btn btn-light-grey">

							Close

						</button>

						<button type="button" class="btn btn-danger remove-event no-display">

							<i class='fa fa-trash-o'></i> Delete Event

						</button>

						<button type='submit' class='btn btn-success save-event'>

							<i class='fa fa-check'></i> Save

						</button>

					</div>

				</div>

			</div>

		</div>



		<!-- start ajax model -->

		<div id="ajax-modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;"></div>

		<!-- end ajax model -->



		<!-- start: MAIN JAVASCRIPTS -->

		<?php $this->load->view("common/footer"); ?>

		<!-- end: MAIN JAVASCRIPTS -->

		<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

		<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>

        <script>

			jQuery(document).ready(function() {

				//TableData.init();

				products_load();

			});


			function print_barcode (product_id) {
				window.open('<?php echo base_url() ?>products/single_barcode/'+product_id, 'sma_popup', 'width=900,height=600,scrollbars=yes,menubar=yes,status=no,resizable=yes,screenx=0,screeny=0');
			}


			function products_load() {



					    $('#products_table').DataTable({

					        "ajax": "<?php echo base_url('products/get_list_product') ?>",

					        "bDestroy": true,

					        "iDisplayLength": 10

					    });



					}

		</script>

	</body>

	<!-- end: BODY -->

</html>