<style type="text/css">
.fa-3x {
  font-size: 2em !important;
}
.report_view_th{
	background-color:#428bca;
	color:#fff !important;
	font-size:14px;	
}
.table-responsive td{
	font-size:14px;	
}
h4{
	font-size:13px;
}
.not-active {
   pointer-events: none;
   cursor: default;
}
</style>
		
		</style>

	<?php $this->load->view("common/header"); ?>
	<!-- end: HEAD -->
     
       
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>plugins/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/DataTables/media/css/DT_bootstrap.css" />
		<link href="<?php echo asset_url(); ?>plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo asset_url(); ?>plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">


		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/select2/select2.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/summernote/build/summernote.css">
        
          <link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>css/jquery.dataTables.css">
          
          
          
          


		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: HEADER -->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<!-- start: TOP NAVIGATION CONTAINER -->
			<div class="container">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="clip-list-2"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<?php $this->load->view("common/logo"); ?>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<!-- start: TOP NAVIGATION MENU -->
				<?php $this->load->view("common/notifications.php"); ?>
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<div class="navbar-content">
				<!-- start: SIDEBAR -->
				<?php $this->load->view("common/navigation"); ?>
				<!-- end: SIDEBAR -->
			</div>
			<!-- start: PAGE -->
			<div class="main-content">
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
                            	<li>
									<a href="<?php echo base_url('dashboard'); ?>">
										 Dashboard 
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('sales'); ?>">
										 Sales 
									</a>
								</li>
                               
								<li class="active">
									View
								</li>
								<li class="search-box">
									<form class="sidebar-search">
										<div class="form-group">
											<input type="text" placeholder="Start Searching...">
											<button class="submit">
												<i class="clip-search-3"></i>
											</button>
										</div>
									</form>
								</li>
							</ol>
							
								
                                <div class="page-header">  
                               <?php  
							   $message = $this->session->flashdata('message');
							   if($message){ ?>
                               	<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert">×</button>
<?php echo $message ?> </div>
                               		
                               <?php
                               }
							    ?>
                               
                                 
							</div>

                            
						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT 
                    <!-- start grid -->
                    <div class="row">
						<div class="col-md-12">
							<!-- start: DYNAMIC TABLE PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Group ID <?php echo $sale_id ?>
									<div class="panel-tools" style="top:2px;">
												<button class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
													<i class="clip-list-5"></i> 
												</button>
												<ul class="dropdown-menu dropdown-light pull-right">
														<li>
															<a id="" data-toggle="modal" href="<?php echo base_url('sales/add'); ?>">
																<i class="fa fa-plus"></i> Add Sales
															</a>
														</li>
														
													</ul>
									</div> <!--panel-tools-->
								</div> <!--panel-heading-->
								<div class="panel-body">
									 
<div class="well well-sm">                      
<?php //print_r($sale_item_list); ?>
<div class="col-xs-4 border-right">
<div class="col-xs-2"><i class="fa fa-3x fa-file-text-o padding010 text-muted"></i></div>
<div class="col-xs-10">
<h4 class="">Group Name: <?php echo $sale_item_list[0]['cname'];?></h4>
<h5 class="">Group ID: <?php echo $sale_item_list[0]['cid'];?></h4>
<p>Date: <?php echo $sale_item_list[0]['added_time'];?></p>
</div>
<div class="clearfix"></div>
</div>

<div class="col-xs-4 border-right" style="visibility:hidden">
<div class="col-xs-2"><i class="fa fa-3x fa-user padding010 text-muted"></i></div>
<div class="col-xs-10">
<h4 class=""><?php // echo $customer_details['cus_name']; ?></h4>
<?php // echo $customer_details['cus_address']; ?><br>
<p></p>
Tel:  <?php // echo $customer_details['cus_phone']; ?><br>
Email: <?php // echo $customer_details['cus_email']; ?> </div>
<div class="clearfix"></div>
</div>

<div class="col-xs-4">
<div class="col-xs-2"><i class="fa fa-3x fa-building-o padding010 text-muted"></i></div>
<div class="col-xs-10">
<h4 class=""><?php echo $warehouse_details['name']; ?></h4>

<?php echo $warehouse_details['address']; ?><p></p>
Tel: <?php echo $warehouse_details['phone']; ?><br>
Email: <?php echo $warehouse_details['email']; ?> </div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div> <!--col-xs-4-->



<div class="col-xs-5">
<input name="sale_id" type="hidden" id="sale_id" value="<?php echo $sale_id ?>">
<input type="hidden" id="sale_type" name="sale_type" value="sale">


<div class="clearfix"></div>
</div> <!--col-xs-4"-->
<div class="clearfix"></div>






<div class="table-responsive">
<table class="table table-bordered table-hover table-striped print-table order-table">
<thead>
<tr class="report_view_th">
<th>No</th>
<th style="text-align:center; padding-right:10px;" colspan="5">Description (Code)</th>
<th style="text-align:center; vertical-align:middle;">Unite Price
<th style="width: 30px !important; text-align: center;"><i class=" "></i></th>
</tr>
</thead>
<tbody>
 <?php 
 $tmpcount=0;
 $tmptot=0;
  foreach ($sale_item_list as $row)
 {
	 $tmpcount++;
	 
	 $tmptot=$tmptot+$row['wholesale_price'];
 ?>  
<tr>
<td style="text-align:center; width:40px; vertical-align:middle;"><?php echo $tmpcount ?></td>
<td colspan="5" style=""><?php echo $row['product_name']; ?> (<?php echo $row['product_code']; ?>)
<?php if ($row['product_part_no']) echo ", Part No.:".$row['product_part_no']; ?>
<?php if ($row['product_oem_part_number']) echo ", OEM Part No.:".$row['product_oem_part_number']; ?>

</td>
 <td style="text-align:right;"> <input class="form-control auto" id="<?php echo $tmpcount ?>" type="text" onClick="this.select()" value="<?php echo $row['wholesale_price'] ?>"></td>
<td>
<button class="btn-warning fa fa-save" style="cursor:pointer" onClick="set_values(<?php echo $tmpcount ?>,<?php echo $sale_item_list[0]['cid'];?>,<?php echo $row['product_id']; ?>,<?php echo $row['wholesale_price'] ?>)" value="<?php echo $row['wholesale_price'] ?>">
</button>
</td>
</tr>
<?php }?>

</tbody>
</table>
</div> <!--table-responsive-->



<div class="clearfix"></div>
<p>
<input type="hidden" id="cid_fld" name="cid_fld" value="">
<input type="hidden" id="pid_fld" name="pid_fld" value="">
<input type="hidden" id="price_fld" name="price_fld" value="">
</p>
<div class="well well-sm col-xs-6 pull-right" style="display:none">


<div class="col-xs-10">
<p>
<?php //echo $tmptot;?><br>

Created by : <?php //print_r( $user_details ); ?> (<?php echo $this->session->userdata('ss_user_group_name'); ?>) </p>

<p>Date:<?php echo display_date_time_format($sale_item_list[0]['added_time'])?></p>
 </div>
<div class="clearfix"></div>

<div class="clearfix"></div>
</div> <!--well well-sm col-xs-6 pull-right-->


<!-- payment list -->


<div class="clearfix"></div>
<br>
<div class="clearfix"></div>
<!-- end payment list-->



<div class="buttons">
<div class="btn-group btn-group-justified">


<div class="btn-group"><a title="" class="tip btn btn-primary tip" href="<?php echo base_url() ?>cus_pricing/add_pricing?g_id=<?php echo $sale_id ?>&w_id=<?php echo $warehouse_details['id'] ?>" data-original-title="Add Payment" ><i class="fa fa-barcode"></i> <span class="hidden-sm hidden-xs">&nbsp; Add Products</span></a></div>

<!--<div class="btn-group"><a title="" class="tip btn btn-primary tip" data-target="#myModal" data-toggle="modal" href="http://sma.tecdiary.org/sales/add_payment/2" data-original-title="Add Payment"><i class="fa fa-money"></i> <span class="hidden-sm hidden-xs">View Payment</span></a></div>-->
<!--<div class="btn-group"><a title="" class="tip btn btn-primary tip" data-target="#myModal" data-toggle="modal" href="http://sma.tecdiary.org/sales/add_delivery/2" data-original-title="Add Delivery"><i class="fa fa-truck"></i> <span class="hidden-sm hidden-xs">Add Delivery</span></a></div>-->

<div style="display:none" class="btn-group" onClick="fbs_click(<?php echo $sale_id; ?>)"><a title="" class="tip btn btn-primary" data-original-title="Print"><i class="fa fa-print"></i> <span class="hidden-sm hidden-xs">Print</span></a>
</div>
 
</div>
</div> <!--buttons-->
                                     
                                   
								</div> <!--panel-body-->
							</div><!--panel-->
							
						</div> <!--col-md-12-->
					</div> <!--row-->

					
                    <!-- end grid -->
                    
					
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<div class="footer clearfix">
			<div class="footer-inner">
				2014 &copy; clip-one by cliptheme.
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
		<!-- start: RIGHT SIDEBAR -->
		<!-- end: RIGHT SIDEBAR -->
		<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title">Event Management</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-light-grey">
							Close
						</button>
						<button type="button" class="btn btn-danger remove-event no-display">
							<i class='fa fa-trash-o'></i> Delete Event
						</button>
						<button type='submit' class='btn btn-success save-event'>
							<i class='fa fa-check'></i> Save
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- start ajax model -->
		<div id="ajax-modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" style="display: none;"></div>
		<!-- end ajax model -->

		<!-- start: MAIN JAVASCRIPTS -->
		<?php $this->load->view("common/footer"); ?>
		<!-- end: MAIN JAVASCRIPTS -->

		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo asset_url(); ?>plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?php echo asset_url(); ?>plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script type="text/javascript" src="<?php echo asset_url(); ?>plugins/bootbox/bootbox.min.js"></script>
		<script type="text/javascript" src="<?php echo asset_url(); ?>plugins/jquery-mockjax/jquery.mockjax.js"></script>
		<script type="text/javascript" src="<?php echo asset_url(); ?>plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo asset_url(); ?>plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="<?php echo asset_url(); ?>js/table-data.js"></script>
		<script src="<?php echo asset_url(); ?>plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="<?php echo asset_url(); ?>plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="<?php echo asset_url(); ?>js/ui-modals.js"></script>
		<script src="<?php echo asset_url(); ?>plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?php echo asset_url(); ?>plugins/select2/select2.min.js"></script>
        
       <script src="<?php echo asset_url(); ?>plugins/jQuery-lib/2.0.3/jquery.min.js"></script>
		<script type="text/javascript" language="javascript" src="<?php echo asset_url(); ?>js/jquery.dataTables.js"></script>

		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				TableData.init();
				//Main.init();
				//TableData.init();
				//UIModals.init();

			});
		</script>
        
<script type="text/javascript" language="javascript" >




var j = jQuery.noConflict();
function loadGrid(){
	alert();
	j('#employee-grid').DataTable().ajax.reload();
}
	jQuery(document).ready(function() {
		var dataTable = j('#employee-grid').DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax":{
				url :"sales/list_sales", // json datasource
				type: "post",  // method  , by default get
				error: function(){  // error handling
					j(".employee-grid-error").html("");
					j("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
					//$("#employee-grid_processing").css("display","none");
				}
			}
		} );
		
	} );
	
function set_values(count,cid,pid,price){
	
//var price = 	$('#'+count+'').val();
/*bootbox.alert('Update Price ?', function () {
update_price(cid,pid,$('#'+count+'').val());
                    });
*/

//bootbox.confirm("Update Price ?", function(result){
	//if(result){
		
	$('#'+count+'').text(price);
	$('#cid_fld').val(cid);
	$('#pid_fld').val(pid);
	$('#price_fld').val(price);
	update_price(count,cid,pid,$('#'+count+'').val());
	
	//}else {resetfun(count);
	//}
//});
        
}
function resetfun(){
this.location.reload();
	}
	
	function update_price(count,cid,pid,price)
  {
      jQuery.ajax({
       type: "POST",
       url: "<?php echo base_url().'cus_pricing/update_price?cid='?>"+cid+"&pid="+pid+"&price="+price+"",
//       data: 'id='+id,
       cache: false,
       success: function(response)
       {
		var price = $('#'+count).val();
		var twoPlacedFloat = parseFloat(price).toFixed(2)
		$('#'+count).val(twoPlacedFloat);
		displayNotice('page','Successfully Updated!');
       }
     });
//	 resetfun();
 }
 
	
	
function fbs_click(id) {
	u=location.href;
	t=document.title;
	window.open('<?php echo base_url() ?>sales/sale_details?sale_id='+id,'sharer','toolbar=0,status=0,width=700,height=700, left=10, top=10,scrollbars=yes');return false;
}
</script>
	</body>
	<!-- end: BODY -->
</html>