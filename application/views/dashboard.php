	<?php $this->load->view("common/header"); ?>
	<!-- end: HEAD -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: HEADER -->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<!-- start: TOP NAVIGATION CONTAINER -->
			<div class="container">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="clip-list-2"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<?php $this->load->view("common/logo"); ?>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<!-- start: TOP NAVIGATION MENU -->
				<?php $this->load->view("common/notifications.php"); ?>
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<div class="navbar-content">
				<!-- start: SIDEBAR -->
					<?php $this->load->view("common/navigation"); ?>
				<!-- end: SIDEBAR -->
			</div>
			<!-- start: PAGE -->
			<div class="main-content">
				<!-- start: PANEL CONFIGURATION MODAL FORM -->
				<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Panel Configuration</h4>
							</div>
							<div class="modal-body">
								Here will be a configuration form
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary">
									Save changes
								</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<i class="clip-home-3"></i>
									<a href="#">
										Home
									</a>
								</li>
								<li class="active">
									Dashboard
								</li>
								<li class="search-box">
									<form class="sidebar-search">
										<div class="form-group">
											<input type="text" placeholder="Start Searching...">
											<button class="submit">
												<i class="clip-search-3"></i>
											</button>
										</div>
									</form>
								</li>
							</ol>
							<div class="page-header">
								<h1>Dashboard <small>overview &amp; stats </small></h1>
							</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o"></i>
									Best Sales (<?php echo date("M-Y"); ?>)
								</div>
							<div id="container1" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-th"></i>
									Quick Links
								</div>
								<div class="panel-body panel-scroll" style="height:auto">
										<div class="col-sm-2">
											<a href="<?php echo base_url('products'); ?>"><button class="btn btn-icon btn-block">
												<i class="fa fa-barcode"></i>
												Products <span class="badge badge-primary"> 4 </span>
											</button></a>
										</div>
										<div class="col-sm-2">
                                        <a href="<?php echo base_url('sales/add'); ?>">
											<button class="btn btn-icon btn-block">
												<i class="fa fa-shopping-cart"></i>
												Add Sales <span class="badge badge-primary"> 4 </span>
											</button></a>
										</div>
										<div class="col-sm-2">
                                        <a href="<?php echo base_url('customers'); ?>">
											<button class="btn btn-icon btn-block">
												<i class="fa fa-shopping-cart"></i>
												Customers <span class="badge badge-primary"> 4 </span>
											</button></a>
										</div>
										<div class="col-sm-2">
                                        <a href="<?php echo base_url('suppliers'); ?>">
											<button class="btn btn-icon btn-block">
												<i class="fa fa-shopping-cart"></i>
												Suppliers <span class="badge badge-primary"> 4 </span>
											</button></a>
										</div>
										<div class="col-sm-2">
                                        <a href="<?php echo base_url('users'); ?>">
											<button class="btn btn-icon btn-block">
												<i class="fa fa-shopping-cart"></i>
												Users <span class="badge badge-primary"> 4 </span>
											</button></a>
										</div>
										<div class="col-sm-2">
                                        <a href="<?php echo base_url('purchases'); ?>">
											<button class="btn btn-icon btn-block">
												<i class="fa fa-cogs"></i>
												GRN <span class="badge badge-primary"> 4 </span>
											</button></a>
										</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
                        <?php //print_r($last_5_sales_list);?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-tasks"></i>
									 Latest Five
								</div>
								<div class="panel-body panel-scroll" style="height:auto">
									<div class="tabbable tabs-left">
												<ul class="nav nav-tabs tab-green" id="myTab3">
													<li class="active">
														<a data-toggle="tab" href="#Sales">
															Sales
														</a>
													</li>
													
													<li class="">
														<a data-toggle="tab" href="#Grn">
															GRN
														</a>
													</li>
													
												</ul>
												<div class="tab-content">
													<div id="Sales" class="tab-pane active">
														<div class="table-responsive">
															<table class="table table-bordered table-hover" id="sample-table-1">
																<thead>
																	<tr>
																		
																		<th>Date</th>
																		<th>Invoice No</th>
																		<th>Customer</th>
																		<th>Grand Total</th>
																		<th>Paid</th>
																		<th>Balance</th>
																		<th>Payment Status</th>
																	</tr>
																</thead>
																<tbody>
                                                                <?php 
																foreach ($last_5_sales_list as $row){
																
																		 $total_paid_amount=$row['total_paid_amount'];
																		 if (empty($total_paid_amount)) {
																			 $pay_st = '<span class="label label-warning">Pending</span>';
																			}else{
																			  if ($total_paid_amount >= $row['sale_total']) {
																				$pay_st = '<span class="label label-success">Paid</span>';
																			  }else{
																				$pay_st = '<span class="label label-info">Partial</span>';
																			  }
																			}	
																	
																	?>
																	<tr>
																		
																		<td><?php echo display_date_time_format($row['sale_datetime']); ?></td>
																		<td><?php echo $row['sale_reference_no']; ?></td>
																		<td><?php echo $row['cus_name']; ?></td>
																		<td> <?php echo number_format($row['sale_total'], 2, '.', ','); ?></td>
																		<td><?php echo number_format($total_paid_amount, 2, '.', ','); ?></td>
																		<td><?php echo number_format($row['sale_total']-$total_paid_amount, 2, '.', ','); ?></td>
																		<td><?php echo $pay_st; ?></td>
																	</tr>
                                                                    <?php }?>
																																		
																</tbody>
															</table>
														</div>

													</div>
													<div id="Grn" class="tab-pane">
													<div class="table-responsive">
															<table class="table table-bordered table-hover" id="sample-table-1">
																<thead>
																	<tr>
																		
																		<th>Date</th>
																		<th>Reference No</th>
																		<th>Supplier</th>
																		<th>Grand Total</th>
																		<th>Paid</th>
																		<th>Balance</th>
																		<th>Payment Status</th>
																	</tr>
																</thead>
																<tbody>
                                                                <?php 
																foreach ($last_5_grn_list as $row){
																
																		 $total_paid_amount=$row['grn_total_paid'];
																		 if (empty($total_paid_amount)) {
																			 $pay_st = '<span class="label label-warning">Pending</span>';
																			}else{
																			  if ($total_paid_amount >= $row['grand_total']) {
																				$pay_st = '<span class="label label-success">Paid</span>';
																			  }else{
																				$pay_st = '<span class="label label-info">Partial</span>';
																			  }
																			}	
																	
																	?>
																	<tr>
																		
																		<td><?php echo display_date_time_format($row['date']); ?></td>
																		<td><?php echo $row['reference_no']; ?></td>
																		<td><?php echo $row['supp_company_name']; ?></td>
																		<td> <?php echo number_format($row['grand_total'], 2, '.', ','); ?></td>
																		<td><?php echo number_format($total_paid_amount, 2, '.', ','); ?></td>
																		<td><?php echo number_format($row['grand_total']-$total_paid_amount, 2, '.', ','); ?></td>
																		<td><?php echo $pay_st; ?></td>
																	</tr>
                                                                    <?php }?>
																																		
																</tbody>
															</table>
														</div>
													</div>
													<div id="Purchases" class="tab-pane">
													3
													</div>
													<div id="Customers" class="tab-pane">
													4
													</div>
													<div id="Suppliers" class="tab-pane">
													5
													</div>
												</div>
											</div>
									</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o"></i>
									Best Sales (<?php echo date("M-Y",strtotime("-1 month")) ?>)
								</div>
							<div id="container2" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
							</div>
							</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-bar-chart-o"></i>
									Best Sales (<?php echo date("M-Y",strtotime("-2 month")) ?>)
								</div>
							<div id="container3" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
							</div>
						</div>
					</div>					
					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<div class="footer clearfix">
			<div class="copyright">
				2015 &copy; stock management system
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
		<!-- start: RIGHT SIDEBAR -->
		<div id="page-sidebar">
			<a class="sidebar-toggler sb-toggle" href="#"><i class="fa fa-indent"></i></a>
			<div class="sidebar-wrapper">
				<ul class="nav nav-tabs nav-justified" id="sidebar-tab">
					<li class="active">
						<a href="#users" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a>
					</li>
					<li>
						<a href="#favorites" role="tab" data-toggle="tab"><i class="fa fa-heart"></i></a>
					</li>
					<li>
						<a href="#settings" role="tab" data-toggle="tab"><i class="fa fa-gear"></i></a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="users">
						<div class="users-list">
							<h5 class="sidebar-title">On-line</h5>
							<ul class="media-list">
								<li class="media">
									<a href="#">
										<i class="fa fa-circle status-online"></i>
										<img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Nicole Bell</h4>
											<span> Content Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<div class="user-label">
											<span class="label label-success">3</span>
										</div>
										<i class="fa fa-circle status-online"></i>
										<img alt="..." src="assets/images/avatar-3.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Steven Thompson</h4>
											<span> Visual Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<i class="fa fa-circle status-online"></i>
										<img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Ella Patterson</h4>
											<span> Web Editor </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<i class="fa fa-circle status-online"></i>
										<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Kenneth Ross</h4>
											<span> Senior Designer </span>
										</div>
									</a>
								</li>
							</ul>
							<h5 class="sidebar-title">Off-line</h5>
							<ul class="media-list">
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Nicole Bell</h4>
											<span> Content Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<div class="user-label">
											<span class="label label-success">3</span>
										</div>
										<img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Steven Thompson</h4>
											<span> Visual Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-8.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Ella Patterson</h4>
											<span> Web Editor </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-9.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Kenneth Ross</h4>
											<span> Senior Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Ella Patterson</h4>
											<span> Web Editor </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Kenneth Ross</h4>
											<span> Senior Designer </span>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<div class="user-chat">
							<div class="sidebar-content">
								<a class="sidebar-back" href="#"><i class="fa fa-chevron-circle-left"></i> Back</a>
							</div>
							<div class="user-chat-form sidebar-content">
								<div class="input-group">
									<input type="text" placeholder="Type a message here..." class="form-control">
									<div class="input-group-btn">
										<button class="btn btn-success" type="button">
											<i class="fa fa-chevron-right"></i>
										</button>
									</div>
								</div>
							</div>
							<ol class="discussion sidebar-content">
								<li class="other">
									<div class="avatar">
										<img src="assets/images/avatar-4.jpg" alt="">
									</div>
									<div class="messages">
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</p>
										<span class="time"> 51 min </span>
									</div>
								</li>
								<li class="self">
									<div class="avatar">
										<img src="assets/images/avatar-1.jpg" alt="">
									</div>
									<div class="messages">
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</p>
										<span class="time"> 37 mins </span>
									</div>
								</li>
								<li class="other">
									<div class="avatar">
										<img src="assets/images/avatar-4.jpg" alt="">
									</div>
									<div class="messages">
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</p>
									</div>
								</li>
							</ol>
						</div>
					</div>
					<div class="tab-pane" id="favorites">
						<div class="users-list">
							<h5 class="sidebar-title">Favorites</h5>
							<ul class="media-list">
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Nicole Bell</h4>
											<span> Content Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<div class="user-label">
											<span class="label label-success">3</span>
										</div>
										<img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Steven Thompson</h4>
											<span> Visual Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Ella Patterson</h4>
											<span> Web Editor </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Kenneth Ross</h4>
											<span> Senior Designer </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Ella Patterson</h4>
											<span> Web Editor </span>
										</div>
									</a>
								</li>
								<li class="media">
									<a href="#">
										<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
										<div class="media-body">
											<h4 class="media-heading">Kenneth Ross</h4>
											<span> Senior Designer </span>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<div class="user-chat">
							<div class="sidebar-content">
								<a class="sidebar-back" href="#"><i class="fa fa-chevron-circle-left"></i> Back</a>
							</div>
							<ol class="discussion sidebar-content">
								<li class="other">
									<div class="avatar">
										<img src="assets/images/avatar-4.jpg" alt="">
									</div>
									<div class="messages">
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</p>
										<span class="time"> 51 min </span>
									</div>
								</li>
								<li class="self">
									<div class="avatar">
										<img src="assets/images/avatar-1.jpg" alt="">
									</div>
									<div class="messages">
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</p>
										<span class="time"> 37 mins </span>
									</div>
								</li>
								<li class="other">
									<div class="avatar">
										<img src="assets/images/avatar-4.jpg" alt="">
									</div>
									<div class="messages">
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</p>
									</div>
								</li>
							</ol>
						</div>
					</div>
					<div class="tab-pane" id="settings">
						<h5 class="sidebar-title">General Settings</h5>
						<ul class="media-list">
							<li class="media">
								<div class="checkbox sidebar-content">
									<label>
										<input type="checkbox" value="" class="green" checked="checked">
										Enable Notifications
									</label>
								</div>
							</li>
							<li class="media">
								<div class="checkbox sidebar-content">
									<label>
										<input type="checkbox" value="" class="green" checked="checked">
										Show your E-mail
									</label>
								</div>
							</li>
							<li class="media">
								<div class="checkbox sidebar-content">
									<label>
										<input type="checkbox" value="" class="green">
										Show Offline Users
									</label>
								</div>
							</li>
							<li class="media">
								<div class="checkbox sidebar-content">
									<label>
										<input type="checkbox" value="" class="green" checked="checked">
										E-mail Alerts
									</label>
								</div>
							</li>
							<li class="media">
								<div class="checkbox sidebar-content">
									<label>
										<input type="checkbox" value="" class="green">
										SMS Alerts
									</label>
								</div>
							</li>
						</ul>
						<div class="sidebar-content">
							<button class="btn btn-success">
								<i class="icon-settings"></i> Save Changes
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end: RIGHT SIDEBAR -->
		<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title">Event Management</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-light-grey">
							Close
						</button>
						<button type="button" class="btn btn-danger remove-event no-display">
							<i class='fa fa-trash-o'></i> Delete Event
						</button>
						<button type='submit' class='btn btn-success save-event'>
							<i class='fa fa-check'></i> Save
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<?php $this->load->view("common/footer"); ?>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo asset_url(); ?>plugins/jquery.sparkline/jquery.sparkline.js"></script>
		<script src="<?php echo asset_url(); ?>plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<?php 

//echo "<pre>"; print_r($current_month_best_sales);
//echo date("m",strtotime("-1 month"));
?>

		<script src="<?php echo asset_url(); ?>/js/highcharts.js"></script>
		<script type="text/javascript">
				$(function () {
           $('#container1').highcharts({
                chart: {type: 'column'},
                title: {text: ''},
                credits: {enabled: false},
                xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
                yAxis: {min: 0, title: {text: ''}},
                legend: {enabled: false},
                series: [{
                    name: 'Sold',
                    data: [
					<?php foreach ($current_month_best_sales as $key => $bestsales) {
						$p_name=preg_replace("/[^a-zA-Z]/", "",$bestsales->product_name);
						?>
					  ['(<?php echo $bestsales->product_code; ?>) <?php echo $p_name ?> ',<?php echo $bestsales->fi_qty_tot; ?>],
					<?php }?>
										
					],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        y: -25,
                        style: {fontSize: '12px'}
                    }
                }]
            });
           $('#container2').highcharts({
                chart: {type: 'column'},
                title: {text: ''},
                credits: {enabled: false},
                xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
                yAxis: {min: 0, title: {text: ''}},
                legend: {enabled: false},
                series: [{
                    name: 'Sold',
                    data: [
					<?php foreach ($last_month_best_sales as $key => $bestsales) { $p_name=preg_replace("/[^a-zA-Z]/", "",$bestsales->product_name);?>
					['(<?php echo $bestsales->product_code; ?>)<?php echo $p_name; ?> <?php if ($bestsales->product_part_no) echo ", Part No: $bestsales->product_part_no"; ?> <?php if ($bestsales->product_oem_part_number) echo ", OEM Part No: $bestsales->product_oem_part_number"; ?>', <?php echo $bestsales->fi_qty_tot; ?>],
					<?php }?>
					],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        y: -25,
                        style: {fontSize: '12px'}
                    }
                }]
            });
			
			
           $('#container3').highcharts({
                chart: {type: 'column'},
                title: {text: ''},
                credits: {enabled: false},
                xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
                yAxis: {min: 0, title: {text: ''}},
                legend: {enabled: false},
                series: [{
                    name: 'Sold',
                    data: [<?php foreach ($last_2_month_best_sales as $key => $bestsales) { $p_name=preg_replace("/[^a-zA-Z]/", "",$bestsales->product_name);?>
					['(<?php echo $bestsales->product_code; ?>) <?php echo $p_name; ?> <?php if ($bestsales->product_part_no) echo ", Part No: $bestsales->product_part_no"; ?> <?php if ($bestsales->product_oem_part_number) echo ", OEM Part No: $bestsales->product_oem_part_number"; ?>', <?php echo $bestsales->fi_qty_tot; ?>],
					<?php }?>],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        y: -25,
                        style: {fontSize: '12px'}
                    }
                }]
            });
			
					});
		</script>

	</body>
	<!-- end: BODY -->
</html>