<?php
 
class Country_Model extends CI_Model {
  
  
  function __construct() 
  {
    /* Call the Model constructor */
    parent::__construct();
  }

	public function get_all_country()
	{
		$query = $this->db->query('SELECT  country_id, country_long_name FROM mstr_country');
        return $this->db->query($query)->result();
	}
	
	public function get_all_areas()
	{
		$query = $this->db->query('SELECT * FROM mstr_city');
        return $query->result_array();
	}
	
	
	public function get_area_info_by_id($id)
	{
		$query = $this->db->query('SELECT * FROM mstr_city WHERE cid ='.$id.' ');
        return $query->result_array();
	}
}