<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos_Model extends CI_Model {

  function __construct() 
  {
    /* Call the Model constructor */
    parent::__construct();
  }

  public function get_product_by_cat_id($category_id='')
  {
     $this->db->select('product_id,product_name,product_code,product_thumb,cat_id,sub_cat_id');
     $this->db->from('product');
     $this->db->where('cat_id',$category_id);
     $this->db->where('product_status',1);
     $query = $this->db->get();
     if($query->num_rows() >0)
     {
       return $query->result();
     }
     else
     {
       return false;
     }

  }

  public function get_all_category()
  {
     $this->db->select('*');
     $this->db->from('product_category');
     $this->db->where('cat_status',1);
     $query = $this->db->get();
     if($query->num_rows() >0)
     {
       return $query->result();
     }
     else
     {
       return false;
     }
  }

   function get_sub_category_by_cat_id($category_id='')
   {
     $this->db->select('*');
     $this->db->from('product_sub_category');
     $this->db->where('cat_id',$category_id);
     $this->db->where('sub_cat_status',1);
     $query = $this->db->get();
     if($query->num_rows() >0)
     {
       return $query->result();
     }
     else
     {
       return false;
     }

   }

   function get_product_by_cat_sub_id($category_id='',$sub_category_id ='')
   {
     $this->db->select('product_id,product_name,product_code,product_thumb,cat_id,sub_cat_id');
     $this->db->from('product');
     $this->db->where('cat_id',$category_id);
     $this->db->where('sub_cat_id',$sub_category_id);
     $this->db->where('product_status',1);
     $query = $this->db->get();
     if($query->num_rows() >0)
     {
       return $query->result();
     }
     else
     {
       return false;
     }
   }

   function get_product_by_code($product_code,$customer_id,$warehouse_id)
   {
     $this->db->select('p.*');
     $this->db->from('product p');
     $this->db->like('p.product_name',$product_code); 
     $this->db->or_like('p.product_code',$product_code); 
     $this->db->or_like('p.product_part_no',$product_code); 
     $this->db->limit('10');
     $query = $this->db->get();
   
     if($query->num_rows() >0)
     {
       return $query->result();
     }
     else
     {
       return false;
     }
   }

	function get_customer()
	{
	  $this->db->select('*');
	  $this->db->from('customer');
	  $this->db->where('cus_status',1);
	     $query = $this->db->get();
	   
	     if($query->num_rows() >0)
	     {
	       return $query->result();
	     }
	     else
	     {
	       return false;
	     }
	}

	function get_warehouse()
	{
	  $this->db->select('*');
	  $this->db->from('warehouses');
	  $this->db->where('status',1);
	     $query = $this->db->get();
	   
	     if($query->num_rows() >0)
	     {
	       return $query->result();
	     }
	     else
	     {
	       return false;
	     }
	}

	public function save_sale_header($sale_ref,$poswarehouse,$customer_id,$sale_date,$payment_note,$grand_total,$pos_discount_input,$discount,$paid_by)
	{
	    $data = array(
	       'sale_reference_no'           => $sale_ref,
	       'warehouse_id'                => $poswarehouse,
	       'customer_id'                 => $customer_id,
         'sale_datetime'               => $sale_date,
	       'sale_note'                   => $payment_note,
	       'sale_total'                  => $grand_total,
	       'sale_inv_discount'           => $pos_discount_input,
	       'sale_inv_discount_amount'    => $discount,
	       'paid_by'                     => $paid_by,
         'sale_datetime_created'       => $sale_date
	    );

	    if($this->db->insert('sales', $data)){
	      return $this->db->insert_id();
	    }else{
	      return false;
	    }
	}

  function sale_items_in($sale_id,$pr_id,$product_code,$product_name,$quantity,$net_price,$ssubtotal)
  {
      $data = array(
         'sale_id'   => $sale_id,
         'product_id'   => $pr_id,
         'product_code'   => $product_code,
         'product_name'       => $product_name,
         'quantity'   => $quantity,
         'unit_price'   => $net_price,
         'gross_total'   => $ssubtotal
      );

      if($this->db->insert('sale_items', $data)){
        return true;
      }else{
        return false;
      }
  }

  function sales_payment($sale_id="",$paid_by="",$pay_amount="",$sale_date="",$payment_note="",$cc_no="",$pcc_holder="",$pcc_type="",$type="")
  {
      $data = array(
         'sale_id'                            => $sale_id,
         'sale_pymnt_paying_by'               => $paid_by,
         'sale_pymnt_amount'                  => $pay_amount,
         'sale_pymnt_date_time'               => $sale_date,
         'sale_pymnt_added_date_time'         => $sale_date,
         'sale_pymnt_crdt_card_no'            => $cc_no,
         'sale_pymnt_crdt_card_holder_name'   => $pcc_holder,
         'sale_pymnt_crdt_card_type'          => $pcc_type,
         'sale_payment_type'                  => $type
      );

      if($this->db->insert('sale_payments', $data)){
        return true;
      }else{
        return false;
      }
  }
}