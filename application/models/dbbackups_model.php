<?php

class Dbbackups_Model extends CI_Model {
  
  
  function __construct() 
  {
    /* Call the Model constructor */
    parent::__construct();
  }

	public function crete_backups()
	{
		
// Load the DB utility class
$this->load->dbutil();

$prefs = array(     
               'format'      => 'zip',             
             'filename'    => 'db_backup.sql'
            );

$backup =& $this->dbutil->backup($prefs); 
	
	//echo $this->db->last_query();
	//die();	  
// Backup your entire database and assign it to a variable
//$backup = $this->dbutil->backup();


// Load the file helper and write the file to your server
$this->load->helper('file');
write_file('./dbbackup/mybackup.zip', $backup);

// Load the download helper and send the file to your desktop
$this->load->helper('download');
force_download('mybackup.zip', $backup);
		
	}

}