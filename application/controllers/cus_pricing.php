<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cus_Pricing extends CI_Controller {

    var $main_menu_name = "pricing";
	var $sub_menu_name = "pricing";

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Cus_Pricing_Model');
		$this->load->model('Supplier_Model');
		$this->load->model('Warehouse_Model');
		$this->load->model('Common_Model');
		$this->load->model('Tax_Rates_Model');
		$this->load->model('Customer_Model');
		$this->load->model('Sales_Return_Model');
		$this->load->model('User_Model');
		$this->load->model('Country_Model');
		
	}
	
	//Sales list page load
	public function index()
	{
		 
		$data['sales'] = $this->Cus_Pricing_Model->get_all_added_areas();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = $this->sub_menu_name;
        $this->load->view('cus_pricing',$data);
	}	
	
	//Sales details view
	public function view()
	{
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = '';
		
		//get sale id
		$sale_id=$this->uri->segment('3');
		$data['sale_item_list']= $this->Cus_Pricing_Model->get_price_list_by_cus_id($sale_id);
//	print_r($data['sale_item_list']);
		$data['sale_details']= $this->Cus_Pricing_Model->get_price_list_by_cus_id($sale_id);
		$data['warehouse_details']= $this->Warehouse_Model->get_warehouse_info($data['sale_item_list'][0]['warehouse_id']);		
		$data['sale_payments_list']= $this->Cus_Pricing_Model->get_sale_payments_by_sale_id($sale_id);
		$data['user_details']=$this->User_Model->get_user_info($data['sale_item_list'][0]['user_id']);
//		echo $data['sale_item_list'][0]['user_id'];
		$old_payment_tot=0;
		$retured_payment_tot=0;
		$retured_payment_msg_this='';
		$old_payments_dis_msg_this='';
		
		//check return payments
		$return_sales_details=$this->Sales_Return_Model->get_return_sale_info_sale_id($sale_id);
		
		
		foreach ($return_sales_details as $row)
		{
	 		//echo "sale id:$row->sale_id";
			//echo "sale_total:$row->sale_total";

				$this_balance_pament=0;
				$this_trn_amt=$row->sl_rtn_total;
				$retured_payment_tot=$retured_payment_tot+$this_trn_amt;

				$retured_payment_msg_this=$retured_payment_msg_this.' -'.$this_trn_amt.' ,';
			
			
 		}
		
		
		$old_payment_tot=$old_payment_tot-$retured_payment_tot;
		
		$data['old_payments']=$old_payment_tot;
		$data['old_payments_dis_msg']="Return Total Amount ($old_payments_dis_msg_this $retured_payment_msg_this)";
		
		$data['sale_id']=$sale_id;
        $this->load->view('cus_pricing_view',$data);
	}

	//Sales add page
	public function add_sale_payments()
	{
		$sale_pymnt_amount=$this->input->post('sale_pymnt_amount');
		$sale_id=$this->input->post('sale_id');
		$sale_pymnt_ref_no=$this->input->post('sale_pymnt_ref_no');
		$sale_pymnt_paying_by=$this->input->post('sale_pymnt_paying_by');
		$sale_pymnt_date_time=$this->input->post('sale_pymnt_date_time');
		$sale_pymnt_date_time_send=date('Y-m-d H:i:s', strtotime($sale_pymnt_date_time));
		$sale_pymnt_cheque_no=$this->input->post('sale_pymnt_cheque_no');
		$sale_pymnt_crdt_card_no=$this->input->post('sale_pymnt_crdt_card_no');
		$sale_pymnt_crdt_card_holder_name=$this->input->post('sale_pymnt_crdt_card_holder_name');
		$sale_pymnt_crdt_card_month=$this->input->post('sale_pymnt_crdt_card_month');
		$sale_pymnt_crdt_card_year=$this->input->post('sale_pymnt_crdt_card_year');
		$sale_pymnt_crdt_card_type=$this->input->post('sale_pymnt_crdt_card_type');
		$sale_type = $this->input->post('sale_type');

		$sale_pymnt_note=$this->input->post('sale_pymnt_note');
		$user_id=$this->session->userdata('ss_user_id');
		$sale_pymnt_added_date_time=date("Y-m-d H:i:s");
		$sale_pymnt_id='';
		
        $this->load->library('form_validation'); //form validation lib
        $this->form_validation->set_rules('sale_pymnt_amount', 'Amount', 'required');
		if($sale_pymnt_paying_by=='Credit Card'){
			$this->form_validation->set_rules('sale_pymnt_crdt_card_type', 'Card Type', 'required');
			$this->form_validation->set_rules('sale_pymnt_crdt_card_no', 'Credit Card No', 'required');
			$this->form_validation->set_rules('sale_pymnt_crdt_card_holder_name', 'Holder Name', 'required');
			$this->form_validation->set_rules('sale_pymnt_crdt_card_month', 'Month', 'required');
			$this->form_validation->set_rules('sale_pymnt_crdt_card_year', 'Year', 'required');
		}
		if($sale_pymnt_paying_by=='Cheque'){
			$this->form_validation->set_rules('sale_pymnt_cheque_no', 'Cheque No', 'required');
		}
		$this->form_validation->set_rules('sale_id', 'System Error', 'required');


        if ($this->form_validation->run() == FALSE)
        {
           $st = array('status' =>0,'validation' => validation_errors());
           echo json_encode($st);
        }
        else
        {
			$data=array(
				'sale_pymnt_amount'=>$sale_pymnt_amount,	
				'sale_pymnt_ref_no'=>$sale_pymnt_ref_no,
				'sale_pymnt_paying_by'=>$sale_pymnt_paying_by,
				'sale_pymnt_date_time'=>$sale_pymnt_date_time_send,
				'sale_pymnt_note'=>$sale_pymnt_note,
				'user_id'=>$user_id,
				'sale_id'=>$sale_id,
				'sale_pymnt_added_date_time'=>$sale_pymnt_added_date_time,
				'sale_pymnt_cheque_no'=>$sale_pymnt_cheque_no,
				'sale_pymnt_crdt_card_no'=>$sale_pymnt_crdt_card_no,
				'sale_pymnt_crdt_card_holder_name'=>$sale_pymnt_crdt_card_holder_name,
				'sale_pymnt_crdt_card_type'=>$sale_pymnt_crdt_card_type,
				'sale_pymnt_crdt_card_month'=>$sale_pymnt_crdt_card_month,
				'sale_pymnt_crdt_card_year'=>$sale_pymnt_crdt_card_year,
				'sale_payment_type' => $sale_type
			);
			
               if ($this->Cus_Pricing_Model->save_sale_payments($data,$sale_pymnt_id)) {
                    $st = array('status' =>1,'validation' =>'Done!');
                    echo json_encode($st);
               } else {
                    $st = array('status' =>0,'validation' =>'error occurred please contact your system administrator');
                    echo json_encode($st);
               }
		}
	}	
	
	//Sales payment page 
	public function payments()
	{
        $data['sale_id'] = $this->input->get('id');
        $data['sale_type'] = $this->input->get('sale_type');
        $this->load->view('models/sales_payment',$data);	
	}
	
	//Sales save 
	//Sales item save
	//Add sales items to 54 table
	public function save_cus_pricing()
	{
		//print_r($this->input->post());
		//$sale_reference_no=$this->input->post('sale_reference_no');
		 $query   		= $this->Cus_Pricing_Model->get_next_ref_no_cus();
        $result  		= $query->row();
       // $sale_reference_no		= sprintf("%05d", $result->sale_id+1);
		
		$sale_id='';
//		$sale_id = $result->sale_id+1;
		$user_id = $this->input->post('user_id');
		$warehouse_id=$this->input->post('warehouse_id');
		$customer_id=$this->input->post('customer_id');
		$rowCount=$this->input->post('rowCount');
		
		$sale_datetime_1=$this->input->post('sale_datetime');
		$sale_datetime=date('Y-m-d H:i:s', strtotime($sale_datetime_1));
		$tax_rate_id=$this->input->post('tax_rate_id');
		$sale_inv_discount=$this->input->post('sale_inv_discount');
		$sale_status=$this->input->post('sale_status');
		$payment_status=$this->input->post('payment_status');
		$sale_shipping=$this->input->post('sale_shipping');
		$sale_payment_term=$this->input->post('sale_payment_term');
		$sale_total=$this->input->post('sale_total');
		$sale_paid=$this->input->post('sale_paid');
		$sale_balance=$this->input->post('sale_balance');
		$cost_total=$this->input->post('cost_total');
		$in_type=$this->input->post('in_type');
		$sale_inv_discount_amount=$this->input->post('sale_inv_discount_amount');
		$sale_datetime_created=date('Y-m-d H:i:s');
		
		$error='';
		$disMsg='';
		$lastid='';
		
		if(!$error){
			/*$data=array(
				//'sale_reference_no'=>$sale_reference_no,
				'warehouse_id'=>$warehouse_id,
				'customer_id'=>$customer_id,
				'warehouse_id'=>$warehouse_id,
				'sale_datetime'=>$sale_datetime,
				//'tax_rate_id'=>$tax_rate_id,
				//'sale_inv_discount'=>$sale_inv_discount,
				//'sale_status'=>$sale_status,
				//'payment_status'=>$payment_status,
				//'sale_shipping'=>$sale_shipping,
				//'sale_payment_term'=>$sale_payment_term,
				'sale_total'=>$sale_total,
				//'sale_paid'=>$sale_paid,
				'cost_total'=>$cost_total,
				'sale_balance'=>$sale_balance,
				'in_type'=>$in_type,
				'sale_datetime_created'=>$sale_datetime_created,
				'sale_inv_discount_amount'=>$sale_inv_discount_amount
			);
			$_insert=$this->Cus_Pricing_Model->save_sales($data,$sale_id);
			$lastid=$this->db->insert_id();
			$sale_id=$lastid;*/
			
			//save prices
			$row=$this->input->post('row');
			$rowCount=$this->input->post('rowCount');
			$data_item=array();
			for($i=1; $i<=$rowCount; $i++){
				//echo "/ $rowCount , Test:".$row[$i]['product_id'][0];
				if(isset($row[$i]['product_id'][0]))
				{					
				$data_item=array(
					//'sale_id'=>$sale_id,
					'product_id'=>$row[$i]['product_id'][0],
					'user_id'=>$user_id,
					//'discount'=>$row[$i]['discount'][0],
					'wholesale_price'=>$row[$i]['gross_total'][0],
					//'item_cost'=>$row[$i]['item_cost'][0],
					'city_id'=>$customer_id,
					//'unit_price'=>$row[$i]['unit_price'][0]+$row[$i]['item_price_p'][0],
					'warehouse_id'=>$warehouse_id,
					'added_time' =>$sale_datetime_created,
//					'discount_val'=>$row[$i]['discount_val'][0], 
//					'gross_total'=>$row[$i]['gross_total'][0]
				);
			$_insert =	$this->Cus_Pricing_Model->save_prices($data_item);
			$lastid=$this->db->insert_id();
			$sale_id=$lastid;
				$itemid=$this->db->insert_id();
				//insert user activity
				$this->Common_Model->add_user_activitie("Added Sale Item, (Id:$itemid)");
				
				//add reford for f4 table
				$type='sale';
				$ref_id=$sale_id;
				$product=$row[$i]['product_id'][0];
				$quantity=$row[$i]['qty'][0];
				$unit_cost=$row[$i]['unit_price'][0];
				$this->Common_Model->add_fi_table($type,$ref_id,$product,$quantity,$unit_cost);
				}
			
			//$_insert=$this->Cus_Pricing_Model->save_prices($data,$sale_id);
			
			//insert user activity
			$this->Common_Model->add_user_activitie("Add Prices, (Cus_Pricing_Id:$sale_id)");
			$disMsg='Prices successfully added';
			
			}
		
		}else {
			
			$disMsg='Please select these before adding any product:'.$disMsg;
		}	
		
		$this->session->set_flashdata('message', 'Sale successfully added!');
		
		echo json_encode(array('sale_id'=>$customer_id,'error'=>$error,'disMsg'=>$disMsg,));
	}

	//Sales reference no jenarate	
	public function get_next_ref_no(){
		$query=$this->Cus_Pricing_Model->get_next_ref_no();
		$result = $query->row();
		//print_r($result);
		$sale_reference_no=sprintf("%05d", $result->sale_id+1);
		$sale_reference_no=$sale_reference_no;
		echo json_encode(array('sale_reference_no'=>$sale_reference_no));
	}
	
	//Sales ger avalable product qty
	public function get_avalable_product_qty(){
		$product_id=$this->input->get('product_id');
		$warehouse_id=$this->input->get('warehouse_id');
		
		$data['total']=$this->Cus_Pricing_Model->get_avalable_product_qty($product_id,$warehouse_id);
		echo json_encode(array('remmnaingQty'=>$data['total']));
	}

	//Sales add form
    public function add_pricing()
    {
		if($this->input->get('w_id') != null ){
		$w_id = $this->input->get('w_id');
		$data['w_id'] = $w_id;
		}
		
		if($this->input->get('g_id') != null ){
		$g_id = $this->input->get('g_id');
		$data['g_id'] = $g_id;
		}
		
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'add_pricing';
		
		//get suppliers list
		$data['suppliers'] = $this->Supplier_Model->get_all_supplier();
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['tax_rates_list'] = $this->Tax_Rates_Model->get_all_tax_rates();
		$data['customer_list'] = $this->Customer_Model->get_all_customers();
		$data['area_list'] = $this->Country_Model->get_all_areas();
		$data['status_list'] = $this->Common_Model->get_all_status();

        $this->load->view('add_pricing',$data);
    }
	
	//Sales product items get
 public function suggestions($value='')
    {
		//print_r($_GET);
		$term=$this->input->get('term');
		$cus_id =$this->input->get('cus_id');
		//echo $cus_id;
		$in_type=$this->input->get('t');
		$data['sales'] = $this->Cus_Pricing_Model->get_products_suggestions($term,$cus_id);
		$json = array();
		//echo "Count:".count($data['sales']);
		//print_r($data['sales']);
		foreach ($data['sales'] as $row)
		{
			//set price
			$price_tmp=0;
			if($in_type=='Cash'){
				$price_tmp=$row['product_price'];
			}
			if($in_type=='Credit'){
				$price_tmp=$row['credit_salling_price'];
			}
			if($in_type=='Wholesale'){
				$price_tmp=$row['wholesale_price'];
			}
			
			$product_name=$row['product_name'];
			$product_code=$row['product_code'];
			$product_part_no=$row['product_part_no'];
			$product_oem_part_number=$row['product_oem_part_number'];
			$product_id=$row['product_id'];
			$product_price=$price_tmp;
			$sendParameters="'$product_id','$product_name','$product_code','$product_price'";
			$sendParameters="$product_id,$product_name,$product_code,$product_price";
			$extraName='';
			$extraName.=", Selling Price: ".number_format($product_price, 2, '.', ',');
			if($product_part_no) $extraName.=", Part No: $product_part_no";
			if($product_oem_part_number) $extraName.=", OEM Part No: $product_oem_part_number";

			 $json_itm=array(
			 		'id'=> $row['product_id'],
					'product_id'=> $row['product_id'],
					'product_code'=> $row['product_code'],
					'product_name'=> $row['product_name'],
					'product_price'=> $price_tmp,
					'product_part_no'=> $row['product_part_no'],
					'item_cost'=> $row['product_cost'],
					'product_oem_part_number'=> $row['product_oem_part_number'],
                    'value'=> $row['product_name']." (".$row['product_code'].")",
                    'label'=> $row['product_name']." (".$row['product_code'].")$extraName"
                    );
					array_push($json,$json_itm);
		}		
		echo json_encode($json);		
    }
	
	//Sale details page
	public function sale_details()
	{
		$sale_type = 0;
		$sale_id=$this->input->get('sale_id');
		$sale_type = $this->input->get('type');
		$data['sale_details']= $this->Cus_Pricing_Model->get_sale_info($sale_id);
		$data['sale_type'] = $sale_type;
		//get sale item list
		$data['sale_item_list']= $this->Cus_Pricing_Model->get_sale_item_list_by_sale_id($sale_id);
		
		$data['customer_details']= $this->Customer_Model->get_customer_info($data['sale_details']['customer_id']);
		$data['warehouse_details']= $this->Warehouse_Model->get_warehouse_info($data['sale_details']['warehouse_id']);
		$data['sale_payments_list']= $this->Cus_Pricing_Model->get_sale_payments_by_sale_id($sale_id);
		
		//get old payments amounts
		$cus_sales_details=$this->Cus_Pricing_Model->get_sale_info_by_customer_id($data['sale_details']['customer_id']);
		//print_r($cus_sales_details);
		$old_payment_tot=0;
		$retured_payment_tot=0;
		$retured_payment_msg_this='';
		$old_payments_dis_msg_this='';
		
		foreach ($cus_sales_details as $row)
		{
	 		//echo "sale id:$row->sale_id";
			//echo "sale_total:$row->sale_total";
			if($row->sale_id!=$sale_id){
			//get paid amount
			$paid_amount=$this->Cus_Pricing_Model->get_total_paid_by_sale_id($row->sale_id);
			if($row->sale_total!=$paid_amount){
				//$this_balance_pament=$row->sale_total-$paid_amount;
				//$old_payment_tot=$old_payment_tot+$this_balance_pament;
				//echo "sale_total:$row->sale_total , ";
				//$old_payments_dis_msg_this=$old_payments_dis_msg_this.''.$this_balance_pament.' ,';
			}
			}
 		}
		
		//check return payments
		$return_sales_details=$this->Sales_Return_Model->get_return_sale_info_sale_id($sale_id);
		
		
		foreach ($return_sales_details as $row)
		{
	 		//echo "sale id:$row->sale_id";
			//echo "sale_total:$row->sale_total";

				$this_balance_pament=0;
				$this_trn_amt=$row->sl_rtn_total;
				$retured_payment_tot=$retured_payment_tot+$this_trn_amt;

				$retured_payment_msg_this=$retured_payment_msg_this.' -'.$this_trn_amt.' ,';
			
			
 		}
		
		
		$old_payment_tot=$old_payment_tot-$retured_payment_tot;
		
		$data['old_payments']=$old_payment_tot;
		$data['old_payments_dis_msg']="Return Total Amount ($old_payments_dis_msg_this $retured_payment_msg_this)";
		
		
		$data['cr_limit_list'] = $this->Common_Model->get_all_cr_limit();
        $this->load->view('models/view_sales',$data);
	}	
	
	//Sales list
	public function list_city()
	{
	$requestData=$this->input->get();
	
	$search_key=$this->input->get('search');
	//print_r($search_key);
	//echo $search_key['value'];
	$search_key_val=$search_key['value'];
	$start=$this->input->get('start');
	$length=$this->input->get('length');
	$columns = array( 
		0 =>'sale_id', 
		1 => 'sale_reference_no',
		2=> 'sale_id',
		3 =>'sale_id', 
		4 => 'sale_id',
		5=> 'sale_id'
	);
	
	$data = array();
	//$sales_tot = $this->Cus_Pricing_Model->get_all_sales();
	$sales = $this->Cus_Pricing_Model->get_all_added_areas();
//print_r($sales);	
	//$totalData = count($sales_tot);
//	$totalFiltered = $totalData;  
	
	foreach ($sales as $row){
		$nestedData=array(); 
		$nestedData[] = $row['cid'];
		$nestedData[] = $row['cname'];
		$nestedData[] = 'Loading..';
	//	$nestedData[] = $row['cname'];
//		$nestedData[] = $row['cus_phone'];
//		$nestedData[] = $row['city_name'];
//		$nestedData[] = $row['cus_type'];
		$actionTxtDisble='';
		$actionTxtEnable='';
		$actionTxtUpdate='';
		$actionTxtDelete='';
		$actionTxtUpdate='<a  data-toggle="modal"
		 href="'.base_url('cus_pricing/view/'.$row['cid']).'" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit customers"><i class="glyphicon fa fa-edit"></i></a> &nbsp;';
		if($row['status']==1){
			$actionTxtDisble = '<a class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Disable customer" onClick="disablePricingData('.$row['cid'].')"><i class="glyphicon fa fa-check"></i></a> &nbsp;';
	}
		if($row['status']==0){
			$actionTxtEnable = '<a class="btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Disable customer" onClick="enablePricingData('.$row['cid'].')"><i class="glyphicon fa fa-minus-circle"></i></a> &nbsp;';
	}
		$actionTxtDelete='<a class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Delete customer" onClick="deletePricngData('.$row['cid'].')">
															<i class="glyphicon fa fa-trash-o"></i></a>';
		//$nestedData[] =$actionTxtUpdate.$actionTxtEnable.$actionTxtDisble.$actionTxtDelete;
		$nestedData[] ='<center>'.$actionTxtUpdate.$actionTxtEnable.$actionTxtDisble.'</center>';
		
	$data[] = $nestedData;
}

	$json_data = array(
//			"draw"            => intval( $requestData['draw'] ),  
			//"recordsTotal"    => intval( $totalData ),  
			//"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data); 
	}
	
	public function create_group()
	{
        $data['id'] = 1;
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'create_customer';
		
        if (isset($_GET['cus_id'])) {
			$cus_id=$_GET['cus_id'];
		}
		else {
			$cus_id='';
		}
		if($cus_id){
			$data['cus_id']=$cus_id;
			$data['type']='E';
			$data['pageName']='UPDATE GROUP';
			$data['btnText']='Update Group';
		//	$data['customer']= $this->Customer_Model->get_customer_info($cus_id);	
		}
		else {
			$data['cus_id']='';
			$data['type']='A';
			$data['pageName']='ADD GROUP';
			$data['btnText']='Add Group';
			$data['customer']=array();
		}
		//$data['country_list'] = $this->Common_Model->get_all_country();
        $this->load->view('models/create_group',$data);	
	}

	public function update_price(){
		
		$cid = $this->input->get('cid');
		$pid = $this->input->get('pid');
		$price = $this->input->get('price');
		
		//echo $price."++";
		
		$result = $this->Cus_Pricing_Model->update_price($cid,$pid,$price);
		return $result;
		//echo json_encode($result);
		}
	
	public function save_group()
	{
		$cus_id=$this->input->post('cus_id');
		$type=$this->input->post('type');
		$cus_name=$this->input->post('cus_name');
		$cus_code=$this->input->post('cus_code');
		$user_id =$this->session->userdata('ss_user_id');
		$this->load->library('form_validation'); //form validation lib
		if($type=='A')
		{
			$this->form_validation->set_rules('cus_code', 'Code', 'required|is_unique[customer.cus_code]');
		}
		else if($type=='E')
		{
			$this->form_validation->set_rules('cus_code', 'Code', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
        {
           $st = array('status' =>0,'validation' => validation_errors());
           echo json_encode($st);
        }
        else
        {
		$data=array(
			'country_id'=>$country_id,
			'cus_id'=>$cus_id,
			'city_name'=>$city_name,	
			'cus_name'=>$cus_name,
			'cus_code'=>$cus_code,
			'user_id' =>$user_id
		);
		
		$_insert=$this->Cus_Pricing_Model->save_group($data,$cus_id);
		$lastid=$this->db->insert_id();

		if($type=='A'){
			if ($lastid) {
				echo json_encode(array('id'=>$lastid,'type'=>$type,'status' =>1));
			} else {
				echo json_encode(array('status'=>'0'));
			}
		}
		if($type=='E'){
			echo json_encode(array('type'=>$type,'status' =>1));
		}	
			
			 //$st = array('status' =>1,'validation' => validation_errors());
            // echo json_encode($st);
		}
	}

	function delete_pricing() {
		$cid	= $this->input->post('cid');
		$this->Cus_Pricing_Model->delete_pricing($cid);
        if ($cid) {
        	echo json_encode(array('id'=>$cid));
        } else {
        	echo json_encode(array('status'=>'error'));
        }
	}
	function disable_pricing() {
		$cid= $this->input->post('cid');
		$this->Cus_Pricing_Model->disable_pricing($cid);
        if ($cid) {
        	echo json_encode(array('id'=>$cid));
        } else {
        	echo json_encode(array('status'=>'error'));
        }
	}
	function enable_pricing() {
		$cid= $this->input->post('cid');
		$this->Cus_Pricing_Model->enable_pricing($cid);
        if ($cid) {
        	echo json_encode(array('id'=>$cid));
        } else {
        	echo json_encode(array('status'=>'error'));
        }
	}	
}