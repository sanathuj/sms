<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Pos extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
		$this->load->model('pos_model');
		$this->load->model('customer_model');
		$this->load->model('common_model');
		$this->load->model('sales_model');
		date_default_timezone_set("Asia/Colombo");
	}

	public function index()
	{
		$data['category_by_id_1'] = $this->pos_model->get_product_by_cat_id(8);
		$data['category'] 		  = $this->pos_model->get_all_category();
		$data['sub_category'] 	  = $this->pos_model->get_sub_category_by_cat_id(8);
		$data['get_customer']     = $this->pos_model->get_customer();
		$data['get_warehouse']    = $this->pos_model->get_warehouse();
		$this->load->view('pos/pos',$data);
	}

	public function ajaxcategorydata($category_id='')
	{
		$category_id = $this->input->get('category_id');

		$out_cat = '';
		$out_sub = '';
		$d = $this->pos_model->get_product_by_cat_id($category_id);
		$s = $this->pos_model->get_sub_category_by_cat_id($category_id);
		if (!empty($d)) {

			foreach ($d as $key => $prod) {
				$out_cat.="<button data-container='body' class='btn-prni btn-default product pos-tip' title='".$prod->product_name."'  value='".$prod->product_code."' type='button' id='product-".$prod->product_id."'><img class='img-rounded' style='width:60px;height:60px;' alt='".$prod->product_name."' src='".asset_url()."uploads/thumbs/".$prod->product_thumb."'><span>".$prod->product_name."</span></button>";	
			}
			if (!empty($s)) {
				foreach ($s as $key => $sub_cat) {
					$out_sub.="<button id='subcategory-".$sub_cat->sub_cat_id."' type='button' value='".$sub_cat->sub_cat_id."' class='btn-prni subcategory' ><img src='".asset_url()."uploads/no-image.jpg' style='width:60px;height:60px;' class='img-rounded img-thumbnail'/><span>".$sub_cat->sub_cat_name."</span></button>";	
				}

				$jproduct = array(
					"products" =>$out_cat,
					"subcategories"=>$out_sub,
					"tcp"=>count($key));
				$ret = json_encode($jproduct);
				echo $ret;

			} else {
				$jproduct = array(
					"products" =>$out_cat,
					"subcategories"=>"",
					"tcp"=>count($key));
				$ret = json_encode($jproduct);
				echo $ret;
			}
		
		}else{
			$jproduct = array(
				"products" =>"<div></div>",
				"subcategories"=>"",
				"tcp"=>0);
			$ret = json_encode($jproduct);
			echo $ret;
		}

	}

	public function ajaxproducts($category_id='',$subcategory_id='',$per_page='')
	{
		$nm = '';
		$category_id 	= $this->input->get('category_id');
		$subcategory_id = $this->input->get('subcategory_id');

		$n = $this->pos_model->get_product_by_cat_sub_id($category_id,$subcategory_id);
		if (!empty($n)) {

			foreach ($n as $key => $pro) {
				$nm.="<button data-container='body' class='btn-prni btn-default product pos-tip' title='' value='".$pro->product_code."' type='button' id='product-".$pro->product_id."'><img class='img-rounded' style='width:60px;height:60px;' alt='".$pro->product_name."' src='".asset_url()."uploads/thumbs/".$pro->product_thumb."'><span>".$pro->product_name."</span></button>";
			}
			echo $nm;

		} else {
			echo "<div></div>";
		}
		
	}

	public function getProductDataByCode()
	{
        $emp_array=array();

		$product_code = $this->input->get('code');
		$customer_id  = $this->input->get('customer_id');
		$warehouse_id = $this->input->get('warehouse_id');

        $get_product_all_by_id  = $this->pos_model->get_product_by_code($product_code,$customer_id,$warehouse_id);
          if (!empty($get_product_all_by_id)) {
            $empar=array();
            foreach ($get_product_all_by_id as $key => $value) {
              $r      = $get_product_all_by_id[$key];
              $lb = $get_product_all_by_id[$key]->product_name;
              $label  = array("id"=>mt_rand(10,10000),"item_id"=>$get_product_all_by_id[$key]->product_id,"label"=>$get_product_all_by_id[$key]->product_code.' | '.$get_product_all_by_id[$key]->product_name,"qty"=>1,'row' =>$r,'value'=>$get_product_all_by_id[$key]->product_name);
              array_push($empar,$label);
            }
              echo json_encode($empar);

          } else {

            echo '[{"id":0,"label":"No matching result found! Product might be out of stock in the selected warehouse.","value":"hg"}]';
          }

	}

	public function pos_submit()
	{
		$customer_id 		= $this->input->post('poscustomer');
		$poswarehouse 		= $this->input->post('poswarehouse');
		$discount 			= $this->input->post('discount');
		$pos_discount_input = $this->input->post('pos_discount_input1');

		$pay_amount 	= $this->input->post('pay_amount');
		$grand_total 	= $this->input->post('grand_total');		
		$paid_by 		= $this->input->post('paid_by');
		$cc_name 		= $this->input->post('cc_name');
		$cc_no 			= $this->input->post('cc_no');
		$pcc_holder 	= $this->input->post('pcc_holder');
		$pcc_type 		= $this->input->post('pcc_type');
		$payment_note 	= $this->input->post('payment_note');

		$sale_date 		= date("Y-m-d H:i:s");
        $query   		= $this->sales_model->get_next_ref_no();
        $result  		= $query->row();
        $sale_ref		= sprintf("%05d", $result->sale_id+1);

        $pr_id 			= $this->input->post('product_id');
        $product_code 	= $this->input->post('product_code');
        $product_name 	= $this->input->post('product_name');
        $net_price 		= $this->input->post('net_price');
        $ssubtotal 		= $this->input->post('ssubtotal');
        $quantity 		= $this->input->post('quantity');

		$sale_id  		= $this->pos_model->save_sale_header($sale_ref,$poswarehouse,$customer_id,$sale_date,$payment_note,$grand_total,$pos_discount_input,$discount,$paid_by);
		if($sale_id){
			for ($i=0; $i < count($pr_id); $i++) { 
				$this->pos_model->sale_items_in($sale_id,$pr_id[$i],$product_code[$i],$product_name[$i],$quantity[$i],$net_price[$i],$ssubtotal[$i]);
			}
		    
		    $t = $this->pos_model->sales_payment($sale_id,$paid_by,$pay_amount,$sale_date,$payment_note,$cc_no,$pcc_holder,$pcc_type,"pos_sale");
		    if ($t == true) {
		    	redirect("../sales/sale_details?type='pos_sale'&&sale_id=".$sale_id."", "refresh");
		    } else {
		    	echo "error";
		    }
		    
		}

	}

}