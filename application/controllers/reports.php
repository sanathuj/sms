<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

    var $main_menu_name = "reports";
	var $sub_menu_name = "suppliers";

	public function __construct()
	{
		parent::__construct();

		
		$this->load->model('Common_Model');
		$this->load->model('Warehouse_Model');
		$this->load->model('Transfer_Model');
		$this->load->model('Sales_Model');
		$this->load->model('Purchases_Model');
		
		$this->load->model('Product_Damage_Model');
		$this->load->model('Sales_Return_Model');
		$this->load->model('Sequerty_Model');
		$this->load->model('Product_Models');
		$this->load->model('Customer_Model');
		
	}
	
	public function index()
	{
		$this->load->model('Supplier_Model');
		$data['suppliers'] = $this->Supplier_Model->get_all_supplier();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = $this->sub_menu_name;
		$this->load->view('rep_reports',$data);
	}
	
		public function payments(){
		$data['main_menu_name'] = 'reports';
		$data['sub_menu_name'] = 'reports/payments';
		$service_type=$this->uri->segment('3');
		$data['service_type']=$service_type;
		$pageName='';
		
		$data['pageName'] =$pageName;
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		//$data['vehicle_list'] = $this->Vehicle_Model->get_all_vehicle();
		$this->load->view('rep_payments',$data);
	}
	
	
public function get_list_payments_for_report()	
{
	       
	        $data = array();
			//print_r($values);
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$srh_type=$this->input->post('srh_type');
		$srh_payment_term=$this->input->post('srh_payment_term');
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->post('srh_from_date')));
		}
		
		
		 $values = $this->Sales_Model->getPaymentsForPrint($srh_warehouse_id,$srh_to_date,$srh_from_date,$srh_type,$srh_payment_term);
		
			$columns = array( 
		0 =>'bkng_id', 
		1 => 'bkng_id',
		2=> 'bkng_id',
		3 =>'bkng_id', 
		4 => 'bkng_id',
		5=> 'bkng_id',
		6=> 'bkng_id',
		7=> 'bkng_id',
		8=> 'bkng_id'
		);
	        if (!empty($values)) {
	            foreach ($values as $users) {
					
	            $row = array();
					$bkng_id=$users->sale_id;
					$paymnt_id=$users->sale_pymnt_id;
					  $row[] = sprintf("%04d", $users->sale_pymnt_id);
					  $row[] = $users->sale_pymnt_date_time;
					   $row[] = $users->sale_reference_no;
					  
					   
					// checked="checked"
					$pymnt_collected='';
					$checked_status='';
					if($pymnt_collected==1) {
						$checked_status='checked=\"checked\"';
					}else {
						$checked_status='';
					}
					/*
					  $row[] = "<label class=\"checkbox-inline\">
										<input id=\"collected_$paymnt_id\" type=\"checkbox\" class=\"flat-red\" value=\"$paymnt_id\" onchange=\"changeColectedStatus($paymnt_id,this.checked)\" $checked_status>
										Collected
									</label>";	
*/
						 $row[] =  $users->user_first_name;
						 $row[] =  $users->sale_payment_type;
						 $row[] =  $users->sale_pymnt_paying_by;
						  $row[] = $users->sale_pymnt_amount;		
                      		$paid=0;
							//$paid=$this->Booking_Model->get_total_paid_by_booking_id($bkng_id);
						//$row[] =number_format($paid, 2, '.', ',');
						
							//$row[] =number_format($users->bkng_tot_amount-$paid, 2, '.', ',');
				
				//$row[]=$actionTxtUpdate.$actionTxtDisble.$actionTxtEnable.$actionTxtPw.$actionTxtDelete;
	
	                $data[] = $row;
	            }


	            $output = array('data' =>$data);
	            echo json_encode($output);
	        }else{
	            $output = array('data' =>'');
	            echo json_encode($output);

	        }

}
	public function print_product_code_popup()
	{
		$data['main_menu_name'] = 'reports';
		$cat_srh=$this->uri->segment(3);
		$sub_cat_srh=$this->uri->segment(4);
		$data['product_list'] = $this->Product_Models->getProductsProduCodePrint($cat_srh,$sub_cat_srh);
		$this->load->view('models/print_product_code_popup',$data);
		
	}
	
		public function print_product_barcode_list_popup()
	{
		$data['main_menu_name'] = 'reports';
		$cat_srh=$this->uri->segment(3);
		$sub_cat_srh=$this->uri->segment(4);
		$data['product_list'] = $this->Product_Models->getProductsProduCodePrint($cat_srh,$sub_cat_srh);
		$this->load->view('models/print_product_barcode_list_popup.php',$data);
		
	}
	
	public function print_product_code_list_popup()
	{
		$data['main_menu_name'] = 'reports';
		$cat_srh=$this->uri->segment(3);
		$sub_cat_srh=$this->uri->segment(4);
		$data['product_list'] = $this->Product_Models->getProductsProduCodePrint($cat_srh,$sub_cat_srh);
		$this->load->view('models/print_product_code_list_popup',$data);
		
	}
	
	public function print_product_code()
	{
		$this->load->model('category_models');
		$data['main_menu_name'] = 'reports';
		$data['sub_menu_name'] = 'print_product_code';
		
		$cat_srh=$this->input->post('cat_srh');
		$sub_cat_srh=$this->input->post('cat_srh');
		
		
		$data['category_list'] 	= $this->category_models->getCategory();
		// $data['sub_category_list']   = $this->category_models->getSubCategory(1);
		
		$this->load->view('rep_product_code_print',$data);
		
		
			
	}
	
	public function get_list_product_for_code_print($value='')
	{
		$cat_srh=$this->input->post('cat_srh');
		$sub_cat_srh=$this->input->post('sub_cat_srh');
		
			$this->load->model('Product_Models');
	        $values = $this->Product_Models->getProductsProduCodePrint($cat_srh,$sub_cat_srh);
	        $data = array();

	        if (!empty($values)) {
	            foreach ($values as $products) {

	            if ($products->product_status == 0) {$k = "btn-warning";$m = "fa-minus-circle";} else {$k = "btn-green";$m = "fa-check";}
				$retVal = (empty($products->sub_cat_name)) ? "--:--" : $products->sub_cat_name ;

	            $row = array();
				
				
				 $row = array();
				
				 
                  $row[] = $products->product_code;
	                $row[] = $products->product_name;
					 $row[] = $products->cat_name;//. " ($products->supp_code)";
					  $row[] = $products->sub_cat_name;
					  
					
	                //$row[] = $transferd_qty;
	                $data[] = $row;
	            }

	            $output = array('data' =>$data);
	            echo json_encode($output);
	        }else{
	            $output = array('data' =>'');
	            echo json_encode($output);

	        }
	}
	
	public function user_activitie(){
		$data['main_menu_name'] = 'reports';
		$data['sub_menu_name'] = 'user_activitie';
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$this->load->view('rep_user_activitie',$data);
	}
	
	public function get_list_user_activitie_for_print($value='')
	{
		$this->load->model('User_Model');
		//print_r($_REQUEST);
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d H:i:s', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d H:i:s', strtotime($this->input->post('srh_from_date')));
		}
		$this->load->model('Sales_Model');
		$columns = array( 
		0 =>'id', 
		1 => 'id',
		2=> 'id',
		3 =>'id', 
		4 => 'id',
	
	);
	$data = array();
	$grn_data = $this->User_Model->get_all_user_activitie_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date);
	$totalData = count($grn_data);
	$totalFiltered = $totalData;  
	
	foreach ($grn_data as $row){
		$nestedData=array(); 
		$id=$row['id'];
		$nestedData[] =$row['details'];
		/*$nestedData[]=$row['page'];*/ 
		$nestedData[] = $row['user_first_name'];
		$nestedData[] =display_date_time_format($row['datetime']);

	$data[] = $nestedData;
}

	$json_data = array(
			//"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data);
	}
	
	public function grn()
	{
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'grn';
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['supplier_list'] = $this->Purchases_Model->get_supplier();
		$this->load->view('rep_grn',$data);
	}
	
	public function get_list_grn_for_print($value='')
	{
		$this->load->model('Purchases_Model');
		//print_r($_REQUEST);
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$srh_supplier_id=$this->input->post('srh_supplier_id');
		$srh_payment_status=$this->input->post('srh_payment_status');
		
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->post('srh_from_date')));
		}
		
		$this->load->model('Sales_Model');
		$columns = array( 
		0 =>'sale_datetime', 
		1 => 'sale_reference_no',
		2=> 'cus_name',
		3 =>'sale_id', 
		4 => 'sale_id',
		5=> 'sale_id'
	);
	$data = array();
	$grn_data = $this->Purchases_Model->get_all_grn_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date,'','',$srh_supplier_id);
	$totalData = count($grn_data);
	$totalFiltered = $totalData;  
	
	foreach ($grn_data as $row){
		
		
		$p_status='';
		$total_paid_amount=$row['grn_total_paid'];
		if (empty($total_paid_amount)) {
		  $pay_st = '<span class="label label-warning">Pending</span>';
		  $p_status='Pending';
		}else{
		  if ($total_paid_amount >= $row['grand_total']) {
			$pay_st = '<span class="label label-success">Paid</span>';
			$p_status='Paid';
		  }else{
			$pay_st = '<span class="label label-info">Partial</span>';
			$p_status='Partial';
		  }
		}
		
		if($srh_payment_status){
			if($srh_payment_status==$p_status)
		{
		$nestedData=array(); 
		$id=$row['id'];
		$nestedData[] =display_date_time_format($row['date']);
		$nestedData[] = $row['reference_no'];
		$nestedData[] = $row['supp_company_name'];	
		$nestedData[]=$pay_st;
		$nestedData[] = number_format($row['grand_total'], 2, '.', '');
		$nestedData[] = number_format($total_paid_amount, 2, '.', '');
		$nestedData[] = number_format($row['grand_total']-$total_paid_amount, 2, '.', '');
		$data[] = $nestedData;
		}}
		else {
			$nestedData=array(); 
		$id=$row['id'];
		$nestedData[] =display_date_time_format($row['date']);
		$nestedData[] = $row['reference_no'];
		$nestedData[] = $row['supp_company_name'];	
		$nestedData[]=$pay_st;
		$nestedData[] = number_format($row['grand_total'], 2, '.', '');
		$nestedData[] = number_format($total_paid_amount, 2, '.', '');
		$nestedData[] = number_format($row['grand_total']-$total_paid_amount, 2, '.', '');
		$data[] = $nestedData;
		}
		
		
		
}

	$json_data = array(
			//"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data);
	}
	
	

	public function print_grn()
	{
		
		$this->load->model('Purchases_Model');
		$this->load->model('Supplier_Model');
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->get('srh_warehouse_id');
		$srh_supplier_id=$this->input->get('srh_supplier_id');
		$srh_payment_status=$this->input->get('srh_payment_status');
		$data['srh_payment_status']=$srh_payment_status;
		//echo "$srh_warehouse_id";
		if($this->input->get('srh_to_date')){
			$srh_to_date=date('Y-m-d H:i:s', strtotime($this->input->get('srh_to_date')));
		}
		if($this->input->get('srh_from_date')){
			$srh_from_date=date('Y-m-d H:i:s', strtotime($this->input->get('srh_from_date')));
		}
		$this->load->model('Sales_Model');
		//$data['sales_list'] = $this->Sales_Model->get_all_sales_for_print_sales();
		$data['grn_list'] = $this->Purchases_Model->get_all_grn_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date,'','',$srh_supplier_id);
		
		$srh_supplier_name='';
		
		if($srh_warehouse_id){
			$warehouse_details=$this->Warehouse_Model->get_warehouse_info($srh_warehouse_id);
			$data['warehouse_details']=$warehouse_details;
			$data['srh_warehouse_name']=$warehouse_details['name'];
		}else {
			$data['srh_warehouse_name']="-All-";
		}
		if($srh_supplier_id){
			$supplier_details=$this->Supplier_Model->get_supplier_info($srh_supplier_id);
			
			$srh_supplier_name=$supplier_details['supp_company_name'];
		}
		$data['srh_supplier_name']=$srh_supplier_name;
		if($srh_to_date){
			$data['srh_to_date_dis']=display_date_time_format($srh_to_date);
		}else {
			$data['srh_to_date_dis']='';	
		}
		if($srh_from_date){
			$data['srh_from_date_dis']=display_date_time_format($srh_from_date);
		}else {
			$data['srh_from_date_dis']='';	
		}
		
		
		
		$this->load->view('models/print_grn',$data);
	}
	

	public function daily_sales()
	{
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'daily_sales';
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$this->load->view('rep_sales_daily',$data);
	}	

	public function sales()
	{
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'sales';
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['customer_list'] = $this->Customer_Model->get_all_customers();
		$this->load->view('rep_sales',$data);
	}
	
	public function print_sale()
	{
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->get('srh_warehouse_id');
		$srh_customer_id=$this->input->get('srh_customer_id');
		//echo "cus id:".$srh_customer_id;
		$srh_payment_status=$this->input->get('srh_payment_status');  
		$data['srh_payment_status']=$srh_payment_status;
		if($this->input->get('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->get('srh_to_date')));
		}
		if($this->input->get('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->get('srh_from_date')));
		}
		$this->load->model('Sales_Model');
		//$data['sales_list'] = $this->Sales_Model->get_all_sales_for_print_sales();
		$data['sales_list'] = $this->Sales_Model->get_all_sales_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date,'','','',$srh_customer_id);
		
		
		
		$srh_customer_name='';
		
		if($srh_customer_id){
			$customer_details=$this->Customer_Model->get_customer_info($srh_customer_id);
			//$data['customer_details']=$customer_details;
			$data['srh_customer_name']=$customer_details['cus_name'];
		}else {
			$data['srh_customer_name']="-All-";
		}
		
		if($srh_warehouse_id){
			$warehouse_details=$this->Warehouse_Model->get_warehouse_info($srh_warehouse_id);
			$data['srh_warehouse_name']=$warehouse_details['name'];
			$data['warehouse_details']=$warehouse_details;
		}else {
			$data['srh_warehouse_name']="-All-";
		}
		if($srh_to_date){
			$data['srh_to_date_dis']=($srh_to_date);
		}else {
			$data['srh_to_date_dis']='';	
		}
		if($srh_from_date){
			$data['srh_from_date_dis']=($srh_from_date);
		}else {
			$data['srh_from_date_dis']='';	
		}
		
		$this->load->view('models/print_sale',$data);
	}
	
	public function suppliers()
	{
		$this->load->model('Supplier_Model');
		$data['suppliers'] = $this->Supplier_Model->get_all_supplier();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = $this->sub_menu_name;
		$this->load->view('rep_suppliers',$data);
	}
	
	public function print_supplier()
	{
		$this->load->model('Supplier_Model');
		$data['suppliers_list'] = $this->Supplier_Model->get_all_supplier();

		$this->load->view('models/print_supplier',$data);
	}
	
	public function products()
	{
		$this->load->model('Product_Models');
		$this->load->model('category_models');
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['product_list'] = $this->Product_Models->getProducts();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'products';
		$data['category_list'] 	= $this->category_models->getCategory();
		$this->load->view('rep_products',$data);
	}
	
	public function products_quantity()
	{
		$this->load->model('Product_Models');
		$this->load->model('category_models');
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['product_list'] = $this->Product_Models->getProducts();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'products_quantity';
		$data['category_list'] 	= $this->category_models->getCategory();
		$this->load->view('rep_products_quantity',$data);
	}
	
	public function supplier_products()
	{
		$this->load->model('Product_Models');
		$this->load->model('purchases_model');
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['product_list'] = $this->Product_Models->getProducts();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'supplier_products';
		$data['supplier_list']=$this->purchases_model->get_supplier();
		
		$this->load->view('rep_supplier_products',$data);
	}
	
	public function alert_quantity()
	{
		$this->load->model('Product_Models');
		$this->load->model('category_models');
		$data['category_list'] 	= $this->category_models->getCategory();
		
		$data['warehouse_list'] = $this->Warehouse_Model->get_all_warehouse();
		$data['product_list'] = $this->Product_Models->getProducts();
		$data['main_menu_name'] = $this->main_menu_name;
		$data['sub_menu_name'] = 'alert_quantity';
		$this->load->view('rep_alert_quantity',$data);
	}
	
	public function print_products()
	{
		$this->load->model('Product_Models');
		
		$data['product_list'] = $this->Product_Models->getProducts();
		$this->load->view('models/print_products',$data);
	}
	
	public function print_alert_quantity()
	{
		$this->load->model('Product_Models');
		
		$data['product_list'] = $this->Product_Models->getProducts();
		$this->load->view('models/print_alert_quantity',$data);
	}

	public function get_list_sales_for_print($value='')
	{
		//print_r($_REQUEST);
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$srh_customer_id=$this->input->post('srh_customer_id');
		$srh_payment_status=$this->input->post('srh_payment_status');
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->post('srh_from_date')));
		}
		
		
		
		$this->load->model('Sales_Model');
		$columns = array( 
		0 =>'sale_datetime', 
		1 => 'sale_reference_no',
		2=> 'cus_name',
		3 =>'sale_id', 
		4 => 'sale_id',
		5=> 'sale_id'
	);
	
	$data = array();

	
	
	$sales = $this->Sales_Model->get_all_sales_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date,'','','',$srh_customer_id);
	//echo $this->db->last_query();
	$totalData = count($sales);
	$totalFiltered = $totalData;  
	
	foreach ($sales as $row){
		$nestedData=array(); 
		$p_status='';
		$pay_st ='';
		$sale_id=$row['sale_id'];
		$total_paid_amount=0;
		$total_paid_amount=$this->Sales_Model->get_total_paid_by_sale_id($sale_id);
		//$total_paid_amount=$row['total_paid_amount']; 
		
		$return_tot_amt=0;
		$return_tot_amt=$this->Sales_Return_Model->get_total_return_by_sale_id($sale_id);
		
		$nestedData[] =display_date_time_format($row['sale_datetime']);
		$nestedData[] = $row['sale_reference_no'];
		$nestedData[] = $row['cus_name'];
		
		
		if (empty($total_paid_amount)) {
		  $pay_st = '<span class="label label-warning">Pending</span>';
		   $p_status='Pending';
		}else{
		  if ($total_paid_amount >= ($row['sale_total']-$return_tot_amt)) {
			$pay_st = '<span class="label label-success">Paid</span>';
			$p_status='Paid';
		  }else{
			$pay_st = '<span class="label label-info">Partial</span>';
			$p_status='Partial';
		  }
		}
		
		if($srh_payment_status){
			if($srh_payment_status==$p_status)
		{
		$nestedData[]=$pay_st;
		//$nestedData[] = number_format($row['cost_total'], 2, '.', '');
		$nestedData[] =number_format($row['sale_total'], 2, '.', '');
		$nestedData[] =$return_tot_amt;
		$nestedData[] = number_format($total_paid_amount, 2, '.', '');
		$nestedData[] = number_format($row['sale_total']-$return_tot_amt-$total_paid_amount, 2, '.', '');

	$data[] = $nestedData;
	
		}}else {
			$nestedData[]=$pay_st;
		//$nestedData[] = number_format($row['cost_total'], 2, '.', '');
		$nestedData[] = number_format($row['sale_total'], 2, '.', '');
		$nestedData[] =$return_tot_amt;
		$nestedData[] = number_format($total_paid_amount, 2, '.', '');
		$nestedData[] = number_format($row['sale_total']-$return_tot_amt-$total_paid_amount, 2, '.', '');

	$data[] = $nestedData;
		}
}

	$json_data = array(
			//"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data);
	}
	

	public function get_list_sales_report_for_print_daily($value='')
	{
		//print_r($_REQUEST);
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->post('srh_from_date')));
		}
		
		
		
		$this->load->model('Sales_Model');
		$columns = array( 
		0 =>'sale_datetime', 
		1 => 'sale_reference_no',
		2=> 'cus_name',
		3 =>'sale_id', 
		4 => 'sale_id',
		5=> 'sale_id'
	);
	
	$data = array();

	
	
	$sales = $this->Sales_Model->get_all_sales_return_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date);
	//echo $this->db->last_query();
	$totalData = count($sales);
	$totalFiltered = $totalData;  
	
	foreach ($sales as $row){
		$nestedData=array(); 
		$sale_id=$row['sale_id'];
		$total_paid_amount=$row['total_paid_amount']; //$this->Sales_Model->get_total_paid_by_sale_id($sale_id);
		$nestedData[] =display_date_time_format($row['sl_rtn_datetime']);
		$nestedData[] = $row['sl_rtn_reference_no'];
		
		
		
		if (empty($total_paid_amount)) {
		  $pay_st = '<span class="label label-warning">Pending</span>';
		}else{
		  if ($total_paid_amount >= $row['sl_rtn_total']) {
			$pay_st = '<span class="label label-success">Paid</span>';
		  }else{
			$pay_st = '<span class="label label-info">Partial</span>';
		  }
		}
		
		$nestedData[]=$pay_st;
		$nestedData[] = number_format($row['cost_total'], 2, '.', '');
		$nestedData[] = number_format($row['sl_rtn_total'], 2, '.', '');
	
		$nestedData[] = number_format($row['sl_rtn_total']-$row['cost_total'], 2, '.', '');

	$data[] = $nestedData;
}

	$json_data = array(
			//"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data);
	}
	
		
	
	public function get_list_sales_for_print_daily($value='')
	{
		//print_r($_REQUEST);
		$srh_to_date='';
		$srh_from_date='';
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->post('srh_from_date')));
		}
		
		
		
		$this->load->model('Sales_Model');
		$columns = array( 
		0 =>'sale_datetime', 
		1 => 'sale_reference_no',
		2=> 'cus_name',
		3 =>'sale_id', 
		4 => 'sale_id',
		5=> 'sale_id'
	);
	
	$data = array();

	
	
	$sales = $this->Sales_Model->get_all_sales_for_report($srh_warehouse_id,$srh_to_date,$srh_from_date);
	//echo $this->db->last_query();
	$totalData = count($sales);
	$totalFiltered = $totalData;  
	
	foreach ($sales as $row){
		$nestedData=array(); 
		$sale_id=$row['sale_id'];
		$total_paid_amount=$row['total_paid_amount']; //$this->Sales_Model->get_total_paid_by_sale_id($sale_id);
		$nestedData[] =display_date_time_format($row['sale_datetime']);
		$nestedData[] = $row['sale_reference_no'];
		
		
		
		if (empty($total_paid_amount)) {
		  $pay_st = '<span class="label label-warning">Pending</span>';
		}else{
		  if ($total_paid_amount >= $row['sale_total']) {
			$pay_st = '<span class="label label-success">Paid</span>';
		  }else{
			$pay_st = '<span class="label label-info">Partial</span>';
		  }
		}
		
		$nestedData[]=$pay_st;
		$nestedData[] = number_format($row['cost_total'], 2, '.', '');
		$nestedData[] = number_format($row['sale_total'], 2, '.', '');
	
		$nestedData[] = number_format($row['sale_total']-$row['cost_total'], 2, '.', '');

	$data[] = $nestedData;
}

	$json_data = array(
			//"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data);
	}
	
	public function get_list_supplier_for_print($value='')
	{
		$this->load->model('Supplier_Model');
		$requestData= $_REQUEST;
		
	
		$columns = array( 
			0 =>'supp_code', 
			0 =>'supp_company_name', 
			1 => 'supp_email',
			2=> 'supp_company_phone',
			3 =>'supp_city', 
			4 => 'country_id',
			5=> 'supp_id'
		);
	
	$data = array();
	$suppliers = $this->Supplier_Model->get_all_supplier();
	$totalData = count($suppliers);
	$totalFiltered = $totalData; 
	//print_r($suppliers);
	
	foreach ($suppliers as $row){
		$nestedData=array(); 
		$nestedData[] =$row['supp_code'];
		$nestedData[] =$row['supp_company_name'];
		$nestedData[] = $row['supp_email'];
		$nestedData[] = $row['supp_company_phone'];
		$nestedData[] =$row['supp_city'];
		$nestedData[] = $row['country_short_name'];
		$data[] = $nestedData;
	 }
	 	$json_data = array(
			//"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

	echo json_encode($json_data); 
	}
	

	public function get_list_supplier_product_for_print($value='')
	{
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$supplier_srh=$this->input->post('supplier_srh');
		
			$this->load->model('Product_Models');
	        $values = $this->Product_Models->getSupplierProductsForReport($srh_warehouse_id,$supplier_srh);
	        $data = array();

	        if (!empty($values)) {
	            foreach ($values as $products) {

	            if ($products->product_status == 0) {$k = "btn-warning";$m = "fa-minus-circle";} else {$k = "btn-green";$m = "fa-check";}
				$retVal = (empty($products->sub_cat_name)) ? "--:--" : $products->sub_cat_name ;

	            $row = array();
				
				//get transferd qty
				$transferd_qty=0;
				$transfer_reseve_qty=0;
				$transferd_qty=$this->Transfer_Model->getTransferdQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$transfer_reseve_qty=$this->Transfer_Model->getTransferResevedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$sold_qty=$this->Sales_Model->getSoldQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$purchased_qty=$this->Purchases_Model->getPurchasedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$product_damaged_qty=$this->Product_Damage_Model->getProductDamagedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$sales_return_qty=$this->Sales_Return_Model->getSalesReturnQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				 
				 $row = array();
				 $balance_qty=$purchased_qty+$transfer_reseve_qty+$sales_return_qty-$sold_qty-$transferd_qty-$product_damaged_qty;
				
				 
                  $row[] = $products->product_code;
	                $row[] = $products->product_name;
					 $row[] = $products->supp_company_name;//. " ($products->supp_code)";
					 if($products->product_part_no){
					  $row[] = $products->product_part_no;
					 }else {
						$row[] = ''; 
					 }
					  $row[] = number_format($purchased_qty, 2, '.', ',');
					   $row[] = number_format($sold_qty, 2, '.', ',');
	                $row[] = number_format($products->product_alert_qty, 2, '.', ',');
					 $row[] = number_format($balance_qty, 2, '.', ',');
					 
					
	                //$row[] = $transferd_qty;
	                $data[] = $row;
	            }

	            $output = array('data' =>$data);
	            echo json_encode($output);
	        }else{
	            $output = array('data' =>'');
	            echo json_encode($output);

	        }
	   }
	   
	   
	
	public function get_list_product_for_print($value='')
	{
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$cat_srh=$this->input->post('cat_srh');
		
		$srh_from_date='';
		$srh_to_date='';
		if($this->input->post('srh_to_date')){
			$srh_to_date=date('Y-m-d', strtotime($this->input->post('srh_to_date')));
		}
		if($this->input->post('srh_from_date')){
			$srh_from_date=date('Y-m-d', strtotime($this->input->post('srh_from_date')));
		}
		
			$this->load->model('Product_Models');
	        $values = $this->Product_Models->getProductsForReport($srh_warehouse_id,$cat_srh);
	        $data = array();

	        if (!empty($values)) {
	            foreach ($values as $products) {

	            if ($products->product_status == 0) {$k = "btn-warning";$m = "fa-minus-circle";} else {$k = "btn-green";$m = "fa-check";}
				$retVal = (empty($products->sub_cat_name)) ? "--:--" : $products->sub_cat_name ;

	            $row = array();
				
				//get transferd qty
				$transferd_qty=0;
				$transfer_reseve_qty=0;
				
				$transferd_qty=$this->Transfer_Model->getTransferdQtyByWarehouseId($srh_warehouse_id,$products->product_id,$srh_from_date,$srh_to_date);
				$transfer_reseve_qty=$this->Transfer_Model->getTransferResevedQtyByWarehouseId($srh_warehouse_id,$products->product_id,$srh_from_date,$srh_to_date);
				$sold_qty=$this->Sales_Model->getSoldQtyByWarehouseId($srh_warehouse_id,$products->product_id,$srh_from_date,$srh_to_date);
				
				$purchased_qty=$this->Purchases_Model->getPurchasedQtyByWarehouseId($srh_warehouse_id,$products->product_id,$srh_from_date,$srh_to_date);
				//$purchased_qty=1;
				$product_damaged_qty=$this->Product_Damage_Model->getProductDamagedQtyByWarehouseId($srh_warehouse_id,$products->product_id,$srh_from_date,$srh_to_date);
				$sales_return_qty=$this->Sales_Return_Model->getSalesReturnQtyByWarehouseId($srh_warehouse_id,$products->product_id,$srh_from_date,$srh_to_date);
				
				$product_balance=0;
				$product_balance=$purchased_qty+$transfer_reseve_qty+$sales_return_qty-$sold_qty-$transferd_qty-$product_damaged_qty;
				
				$sale_price_sub_tot=$products->product_price*$product_balance;
				$cost_price_sub_tot=$products->product_cost*$product_balance;				 
                  $row[] = $products->product_code;
	                $row[] = $products->product_name;
					 $row[] = $products->product_part_no;
					  $row[] = $products->cat_name;
					   $row[] = $products->sub_cat_name;
	                $row[] = number_format($purchased_qty, 2, '.', ',');
					 $row[] = number_format($sold_qty, 2, '.', ',');
					  $row[] = number_format($sales_return_qty, 2, '.', ',');
					 $row[] = number_format($product_damaged_qty, 2, '.', ',');
	               $row[] = $sale_price_sub_tot;
				   $row[] = $cost_price_sub_tot;
					//$row[] = number_format($transferd_qty, 2, '.', ',');
					//$row[] = number_format($transfer_reseve_qty, 2, '.', ',');
	                $row[] = $product_balance;
	                //$row[] = $transferd_qty;
	                $data[] = $row;
	            }

	            $output = array('data' =>$data);
	            echo json_encode($output);
	        }else{
	            $output = array('data' =>'');
	            echo json_encode($output);

	        }
	   }
	   
	   
	   
	   	public function get_list_product_qty_for_print($value='')
	{
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$cat_srh=$this->input->post('cat_srh');
		
			$this->load->model('Product_Models');
	        $values = $this->Product_Models->getProductsForQTYReport($srh_warehouse_id,$cat_srh);
	        $data = array();

	        if (!empty($values)) {
	            foreach ($values as $products) {

	            if ($products->product_status == 0) {$k = "btn-warning";$m = "fa-minus-circle";} else {$k = "btn-green";$m = "fa-check";}
				$retVal = (empty($products->sub_cat_name)) ? "--:--" : $products->sub_cat_name ;

	            $row = array();
				
				//get transferd qty
				$transferd_qty=0;
				$transfer_reseve_qty=0;
				$transferd_qty=$this->Transfer_Model->getTransferdQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$transfer_reseve_qty=$this->Transfer_Model->getTransferResevedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$sold_qty=$this->Sales_Model->getSoldQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$purchased_qty=$this->Purchases_Model->getPurchasedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$product_damaged_qty=$this->Product_Damage_Model->getProductDamagedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$sales_return_qty=$this->Sales_Return_Model->getSalesReturnQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				 
                  $row[] = $products->product_code;
	                $row[] = $products->product_name;
					// $row[] = $products->product_part_no;
					  $row[] = $products->cat_name;
					   $row[] = $products->sub_cat_name;
	                
					$tmp_qty=$purchased_qty+$transfer_reseve_qty+$sales_return_qty-$sold_qty-$transferd_qty-$product_damaged_qty;
					 $row[] = number_format(($products->product_cost), 2, '.', '');
					 $row[] = number_format(($products->product_price), 2, '.', '');
					 
	                $row[] = number_format($tmp_qty, 2, '.', '');
					
					  $row[] = number_format(($products->product_cost*$tmp_qty), 2, '.', '');
					 $row[] = number_format(($products->product_price*$tmp_qty), 2, '.', '');
					
					
	                //$row[] = $transferd_qty;
	                $data[] = $row;
	            }

	            $output = array('data' =>$data);
	            echo json_encode($output);
	        }else{
	            $output = array('data' =>'');
	            echo json_encode($output);

	        }
	   }
	   
	   
	   public function get_list_product_alert_quantity_for_print($value='')
	{
		$srh_warehouse_id=$this->input->post('srh_warehouse_id');
		$cat_srh=$this->input->post('cat_srh');
		
			$this->load->model('Product_Models');
	        $values = $this->Product_Models->getProductsForReport($srh_warehouse_id,$cat_srh);
	        $data = array();

	        if (!empty($values)) {
	            foreach ($values as $products) {

	            if ($products->product_status == 0) {$k = "btn-warning";$m = "fa-minus-circle";} else {$k = "btn-green";$m = "fa-check";}
				$retVal = (empty($products->sub_cat_name)) ? "--:--" : $products->sub_cat_name ;

	            
				
				//get transferd qty
				$transferd_qty=0;
				$transfer_reseve_qty=0;
				$transferd_qty=$this->Transfer_Model->getTransferdQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$transfer_reseve_qty=$this->Transfer_Model->getTransferResevedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$sold_qty=$this->Sales_Model->getSoldQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$purchased_qty=$this->Purchases_Model->getPurchasedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$product_damaged_qty=$this->Product_Damage_Model->getProductDamagedQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				$sales_return_qty=$this->Sales_Return_Model->getSalesReturnQtyByWarehouseId($srh_warehouse_id,$products->product_id);
				 
				 $row = array();
				 $balance_qty=$purchased_qty+$transfer_reseve_qty+$sales_return_qty-$sold_qty-$transferd_qty-$product_damaged_qty;
                  if($balance_qty<=$products->product_alert_qty){
				  
				  $row[] = $products->product_code;
	                $row[] = $products->product_name;
					 $row[] = $products->product_part_no;
					 $row[] = $products->cat_name;
					 $row[] = $products->sub_cat_name;
					  $row[] = $products->product_alert_qty;
					  $row[] = $products->product_max_qty;
					  
	               
	                $row[] = number_format(($balance_qty), 2, '.', ',');
	                //$row[] = $transferd_qty;
	                $data[] = $row;
				  }
	            }

	            $output = array('data' =>$data);
	            echo json_encode($output);
	        }else{
	            $output = array('data' =>'');
	            echo json_encode($output);

	        }
	   }
	
}